/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.*;
import java.io.*;
import Operations.Players;


/**
 *
 * @author apprentice
 */
public class ReadAndWrite11 {

    Operations.League AB = new Operations.League();

    public void writeFile(String filename, ArrayList<Players> adds) {
        try {
            PrintWriter output = new PrintWriter(new FileWriter(filename));
            String outString;
            Players element;
            Iterator<Players> iter = adds.iterator();
            while (iter.hasNext()) {
                element = iter.next();
                outString = element.getFirstName()+ "::"
                         + element.getLastName()) + "::"
                         + element.getPosition();
                output.println(outString);
                outString = "";
            }
            output.flush();
            output.close();
        } catch (IOException e) {
            System.out.println("File Write failed: " + e.getMessage());
        }
    }

    public ArrayList<Players> readFile(String filename) {
        ArrayList<Players> adds = new ArrayList<>();
        try {
            Scanner file = new Scanner(new BufferedReader(new FileReader(filename)));
            String line;
            String element;
            String[] splitLine;
            while (file.hasNextLine()) {
                line = file.nextLine();
                splitLine = line.split("::");
                Players temp = new Players(splitLine[0],
                        splitLine[1]);
                adds.add(temp);
            }
            file.close();
        } catch (FileNotFoundException e) {
            //either makes a populated or empty address book
        }
        return adds;
    }
}

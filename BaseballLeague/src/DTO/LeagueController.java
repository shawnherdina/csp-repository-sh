/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DAO.ReadAndWrite11;
import Operations.League;
import java.util.ArrayList;
import java.io.*;
import Operations.Players;
import Operations.Teams;

/**
 *
 * @author apprentice
 */
public class LeagueController {

    UI.ConsoleIO111 IO = new UI.ConsoleIO111();
    League LG = new League();
    ReadAndWrite11 RW = new ReadAndWrite11();

    public void mainMenuMethod() {
        ArrayList reader = RW.readFile("league.txt");
        LG.addLeagueViaReader(reader);
        int answer = 0;

        do {

            answer = IO.readInteger("Please Select the Corrsponding Number of Your Choice: \n"
                    + "1. Add Player\n"
                    + "2. Remove Player\n"
                    + "3. Trade Player\n"
                    + "4. List Player\n"
                    + "5. Create a Team\n"
                    + "6. List Team\n"
                    + "7. Exit", 1, 7);
            System.out.println();

            switch (answer) {

                case 1:
                    addPlayer();

                    break;

                case 2:
                    removePlayer();

                    break;

                case 3:
                    tradePlayer();

                    break;

                case 4:
                    listPlayer();

                    break;

                case 5:
                    addTeam();

                    break;

                case 6:
                    listTeams();

                    break;

                default:
            }

        } while (answer != 7);
        if (answer == 7) {
            RW.writeFile("dvdLibrary.txt", LG.allPlayers());
        }
    }

    public void addPlayer() {
        String fname, lname,pos;

        fname = IO.readString("Please Enter the Players First Name: ");
        lname = IO.readString("Please Enter the Players Last Name: ");
        pos=IO.readString("Please Enter the Players Position: ");
        

        System.out.println();
        Players temp = new Players(fname, lname);
        LG.addPlayers(temp);

    }

    public void removePlayer() {
        String title;
        int choice = 0;

        title = IO.readString("Please Enter the Player you'd like to Remove: ");
        Players remove;
        remove = LG.findPlayers(title);

        if (remove == null) {
            IO.write("There is no Player with that Name.");
            System.out.println();
        } else {
            IO.write(remove.getFirstName() + " " + remove.getLastName());
            System.out.println();

            choice = IO.readInteger("Are you sure you want to Delete??\n"
                    + "Press 0 to Delete");

            if (choice == 0) {
                LG.removePlayers(remove);
            }
        }

    }

    public void tradePlayer() {
        String trade, fname, lname;
        int choice = 0;

        trade = IO.readString("Please Enter Players Last Name you'd like to Trade: ");
        Players edit;
        edit = LG.findPlayers(trade);
        
         if (trade == null) {
            IO.write("There is no Player with that Name.");
            System.out.println();
        } else {
            IO.write(trade.getFirstName() + " " + trade.getLastName());
            System.out.println();

            choice = IO.readInteger("Are you sure you want to Trade??\n"
                    + "Press 0 to Delete");

            if (choice == 0) {
                LG.removePlayers(trade);
            }
        }

    }

    public void listPlayer() {
        ArrayList<Players> all = new ArrayList<>();
        all = LG.allPlayers();

        for (int i = 0; i < all.size(); i++) {
            Players temp = all.get(i);

            IO.write(temp.getFirstName() + " " + temp.getLastName() + ", Position: " + temp.getPosition());
            System.out.println();
        }
        System.out.println();
    }
     public void addTeam() {
        String city, mascot;

        city = IO.readString("Please Enter the Teams City: ");
        mascot = IO.readString("Please Enter the Teams Mascot: ");

        System.out.println();
        Teams temp = new Teams(city, mascot);
        LG2.addTeams(temp);

    }
      public void listTeams() {
        ArrayList<Teams> all = new ArrayList<>();
        all = LG.allTeams();

        for (int i = 0; i < all.size(); i++) {
            Teams temp = all.get(i);

            IO.write(temp.getCity()+ ", " + temp.getMascot());
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args) {
        LeagueController LC = new LeagueController();
        LC.mainMenuMethod();

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operations;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class League {

    ArrayList<Players> league = new ArrayList<>();
    ArrayList<Teams> league2 = new ArrayList<>();

    public void addPlayers(Players a) {
        league.add(a);
    }

    public void removePlayers(Players r) {
        league.remove(r);

    }

    public Players findPlayers(String title) {
        Players reqPlayers = null;

        for (int i = 0; i < league.size(); i++) {
            Players temp = league.get(i);

            if (temp.getLastName().equals(title)) {
                reqPlayers = temp;
            }
        }
        return reqPlayers;

    }

    public ArrayList allPlayers() {
        ArrayList tempArrL = (ArrayList) league.clone();
        return tempArrL;
    }

    public void addLeagueViaReader(ArrayList<Players> d) {
        league.addAll(d);
    }

    public ArrayList allTeams() {
        ArrayList tempArrL2 = (ArrayList) league2.clone();
        return tempArrL2;
    }

    public Teams findTeams(String title) {
        Teams reqTeams = null;

        for (int i = 0; i < league2.size(); i++) {
            Teams temp = league2.get(i);

            if (temp.getMascot().equals(title)) {
                reqTeams = temp;
            }
        }
        return reqTeams;

    }
}

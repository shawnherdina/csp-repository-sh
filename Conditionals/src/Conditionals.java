
public class Conditionals {

    public boolean AreWeInTrouble(boolean aSmile, boolean bSmile) {
        return aSmile == bSmile;
    }

    public boolean CanSleepIn(boolean isWeekday, boolean isVacation) {
        return isWeekday == false || isVacation == true;
    }

    public int SumDouble(int a, int b) {
        int sum;
        sum = a + b;
        if (a == b) {
            return sum * 2;
        } else {
            return sum;
        }

    }

    public int Diff21(int n) {
        int diff;
        diff = n - 21;

        if (n >= 21) {
            return diff * 2;
        } else {
            return diff;
        }
    }

    public boolean ParrotTrouble(boolean isTalking, int hour) {
        return (hour > 20 || hour < 7) && isTalking == true;
    }

    public boolean Makes10(int a, int b) {
        return a == 10 || b == 10 || (a + b) == 10;
    }

    public boolean NearHundred(int n) {
        return n <= 190 && n >= 210 && n <= 90 && n >= 110;
    }

    public boolean PosNeg(int a, int b, boolean negative) {
        if (negative == true && a < 0 && b < 0) {
            return true;
        } else {
            return (a > 0 && b < 0) || (a < 0 && b > 0);
        }

    }

    public String NotString(String s) {
        String firstThree;
        firstThree = s.substring(0, 3);
        if (firstThree.equals("not")) {
            return s;
        } else {
            return "not" + " " + s;

        }
    }

    public String MissingChar(String str, int n) {
        String aString;
        String bString;
        aString = str.substring(0, n);
        bString = str.substring(n + 1, str.length());
        return aString + bString;
    }

    public String FrontBack(String str) {
        String aString, bString, cString;
        aString = str.substring(0, 1);
        bString = str.substring(1, str.length() - 1);
        cString = str.substring(str.length() - 1, str.length());

        if (str.length() == 1) {
            return str;
        } else {
            return cString + bString + aString;
        }

    }

    public String Front3(String str) {
        String aString, bString;
        aString = str.substring(0, 2);
        bString = str.substring(0, 1);
        if (str.length() <= 2) {
            return bString + bString + bString;
        } else {
            return aString + aString + aString;
        }
    }

    public String BackAround(String str) {
        String aString;
        aString = str.substring(str.length() - 1, str.length());
        return aString + str + aString;

    }
}

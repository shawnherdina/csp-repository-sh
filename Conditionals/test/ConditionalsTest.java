/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ConditionalsTest {
    
    public ConditionalsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of AreWeInTrouble method, of class Conditionals.
     */
    @Test
    public void testAreWeInTrouble() {
        System.out.println("AreWeInTrouble");
        boolean aSmile = false;
        boolean bSmile = false;
        Conditionals instance = new Conditionals();
        boolean expResult = false;
        boolean result = instance.AreWeInTrouble(true, true);
        assertEquals(true, result);
        result = instance.AreWeInTrouble(false, false);
        assertEquals(true, result);
        result = instance.AreWeInTrouble(true, false);
        assertEquals(false, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of CanSleepIn method, of class Conditionals.
     */
    @Test
    public void testCanSleepIn() {
        System.out.println("CanSleepIn");
        boolean isWeekday = false;
        boolean isVacation = false;
        Conditionals instance = new Conditionals();
        boolean expResult = false;
        boolean result = instance.CanSleepIn(false, false);
        assertEquals(true, result);
        result = instance.CanSleepIn(true, false);
        assertEquals(false, result);
        result = instance.CanSleepIn(false, true);
        assertEquals(true, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of SumDouble method, of class Conditionals.
     */
    @Test
    public void testSumDouble() {
        System.out.println("SumDouble");
        int a = 0;
        int b = 0;
        Conditionals instance = new Conditionals();
        int expResult = 0;
        int result = instance.SumDouble(1,2);
        assertEquals(3, result);
        result = instance.SumDouble(3,2);
        assertEquals(5, result);
        result = instance.SumDouble(2,2);
        assertEquals(8, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of Diff21 method, of class Conditionals.
     */
    @Test
    public void testDiff21() {
        System.out.println("Diff21");
        int n = 0;
        Conditionals instance = new Conditionals();
        int expResult = 0;
        int result = instance.Diff21(23);
        assertEquals(4, result);
        result = instance.Diff21(10);
        assertEquals(11, result);
        result = instance.Diff21(21);
        assertEquals(0, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ParrotTrouble method, of class Conditionals.
     */
    @Test
    public void testParrotTrouble() {
        System.out.println("ParrotTrouble");
        boolean isTalking = false;
        int hour = 0;
        Conditionals instance = new Conditionals();
        boolean expResult = false;
        boolean result = instance.ParrotTrouble(true,6);
        assertEquals(true, result);
        result = instance.ParrotTrouble(true, 7);
        assertEquals(false, result);
        result = instance.ParrotTrouble(false, 6);
        assertEquals(false, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of Makes10 method, of class Conditionals.
     */
    @Test
    public void testMakes10() {
        System.out.println("Makes10");
        int a = 0;
        int b = 0;
        Conditionals instance = new Conditionals();
        boolean expResult = false;
        boolean result = instance.Makes10(9,10);
        assertEquals(true, result);
        result = instance.Makes10(9,9);
        assertEquals(false, result);
        result = instance.Makes10(1,9);
        assertEquals(true, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of NearHundred method, of class Conditionals.
     */
    @Test
    public void testNearHundred() {
        System.out.println("NearHundred");
        int n = 0;
        Conditionals instance = new Conditionals();
        boolean expResult = false;
        boolean result = instance.NearHundred(103);
        assertEquals(true, result);
        result = instance.NearHundred(90);
        assertEquals(true, result);
        result = instance.NearHundred(89);
        assertEquals(false, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of PosNeg method, of class Conditionals.
     */
    @Test
    public void testPosNeg() {
        System.out.println("PosNeg");
        int a = 0;
        int b = 0;
        boolean negative = false;
        Conditionals instance = new Conditionals();
        boolean expResult = false;
        boolean result = instance.PosNeg(1,-1,false);
        assertEquals(true, result);
        result = instance.PosNeg(-1,1,false);
        assertEquals(true, result);
        result = instance.PosNeg(-4,-5,true);
        assertEquals(true, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of NotString method, of class Conditionals.
     */
    @Test
    public void testNotString() {
        System.out.println("NotString");
        String s = "";
        Conditionals instance = new Conditionals();
        String expResult = "";
        String result = instance.NotString("candy");
        assertEquals("not candy", result);
        result = instance.NotString("x");
        assertEquals("not x", result);
        result = instance.NotString("not bad");
        assertEquals("not bad", result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of MissingChar method, of class Conditionals.
     */
    @Test
    public void testMissingChar() {
        System.out.println("MissingChar");
        String str = "";
        int n = 0;
        Conditionals instance = new Conditionals();
        String expResult = "";
        String result = instance.MissingChar("kitten",1);
        assertEquals("ktten", result);
        result = instance.MissingChar("kitten",0);
        assertEquals("itten", result);
        result = instance.MissingChar("kitten",4);
        assertEquals("kittn", result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of FrontBack method, of class Conditionals.
     */
    @Test
    public void testFrontBack() {
        System.out.println("FrontBack");
        String str = "";
        Conditionals instance = new Conditionals();
        String expResult = "";
        String result = instance.FrontBack("code");
        assertEquals("eodc", result);
        result = instance.FrontBack("a");
        assertEquals("a", result);
        result = instance.FrontBack("ab");
        assertEquals("ba", result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of Front3 method, of class Conditionals.
     */
    @Test
    public void testFront3() {
        System.out.println("Front3");
        String str = "";
        Conditionals instance = new Conditionals();
        String expResult = "";
        String result = instance.Front3("Microsoft");
        assertEquals("MicMicMic", result);
        result = instance.Front3("Chocolate");
        assertEquals("ChoChoCho", result);
        result = instance.Front3("at");
        assertEquals("atatat", result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of BackAround method, of class Conditionals.
     */
    @Test
    public void testBackAround() {
        System.out.println("BackAround");
        String str = "";
        Conditionals instance = new Conditionals();
        String expResult = "";
        String result = instance.BackAround("cat");
        assertEquals("tcatt", result);
        result = instance.BackAround("Hello");
        assertEquals("oHelloo", result);
        result = instance.BackAround("a");
        assertEquals("aaa", result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdmvc.dao;

import com.mycompany.dvdmvc.model.DVD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DVDListDaoDbImpl implements DVDListDao {
    private static final String SQL_INSERT_DVD
            = "insert into dvds (movie_title, mpaa, release_date, director, studio, notes) values (?, ?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_DVD
            = "delete from dvds where dvd_id = ?";

    private static final String SQL_SELECT_DVD
            = "select * from dvds where dvd_id = ?";

    private static final String SQL_SELECT_ALL_DVDS
            = "select * from dvds";

    private static final String SQL_UPDATE_DVD
            = "update dvds set movie_title = ?, mpaa = ?, release_date = ?, director = ?, studio = ?,notes = ? where dvd_id = ?";

    private JdbcTemplate jdbcTemplate;

    public void setjdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public DVD addDVD(DVD dvd) {
        jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getMovieTitle(),
                dvd.getMpaa(),
                dvd.getReleaseDate(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getNotes());
        dvd.setDvdid(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return dvd;
    }

    @Override
    public void removeDVD(int dvdid) {
        jdbcTemplate.update(SQL_DELETE_DVD, dvdid);
    }

    @Override
    public void updateDVD(DVD dvd) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getMovieTitle(),
                dvd.getMpaa(),
                dvd.getReleaseDate(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getNotes(),
                dvd.getDvdid());
    }

    @Override
    public List<DVD> getAllDVDs() {
        return jdbcTemplate.query(SQL_SELECT_ALL_DVDS, new DVDMapper());
    }

    @Override
    public DVD getdvdid(int dvdid) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_DVD, new DVDMapper(), dvdid);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<DVD> searchDVD(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllDVDs();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from dvds where ");

            // build the where clause
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];

            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();
            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }

            return jdbcTemplate.query(sQuery.toString(), new DVDMapper(), paramVals);
        }
    }

    private static final class DVDMapper implements ParameterizedRowMapper<DVD> {

        @Override
        public DVD mapRow(ResultSet rs, int i) throws SQLException {
            DVD dvd = new DVD();
            dvd.setDvdid(rs.getInt("dvd_id"));
            dvd.setMovieTitle(rs.getString("movie_title"));
            dvd.setMpaa(rs.getString("mpaa"));
            dvd.setReleaseDate(rs.getString("release_date"));
            dvd.setDirector(rs.getString("director"));
            dvd.setStudio(rs.getString("studio"));
            dvd.setNotes(rs.getString("notes"));
            return dvd;
        }

    }

}

    


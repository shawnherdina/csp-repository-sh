package com.mycompany.dvdmvc.model;

import java.util.Objects;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class DVD {

    private int dvdid;
    
    @NotEmpty(message="You must supply a value for Movie Title.")
    @Length(max=50, message="Movie Title must be no more than 50 characters in length.")
    private String movieTitle;
    
    @NotEmpty(message="You must supply a value for MPAA.")
    @Length(max=5, message="MPAA must be no more than 5 characters in length.")
    private String mpaa;
    
    @NotEmpty(message="You must supply a value for Release Date.")
    @Length(max=15, message="Release Date must be no more than 10 characters in length.")
    private String releaseDate;
    
    @NotEmpty(message="You must supply a value for Studio.")
    @Length(max=50, message="Studio must be no more than 50 characters in length.")
    private String studio;
    
    @NotEmpty(message="You must supply a value for Director.")
    @Length(max=50, message="Director must be no more than 50 characters in length.")
    private String director;
    
    @NotEmpty(message="You must supply a value for Notes.")
    @Length(max=50, message="Notes must be no more than 50 characters in length.")
    private String notes;


    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
    
    public int getDvdid() {
        return dvdid;
    }

    public void setDvdid(int dvdid) {
        this.dvdid = dvdid;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMpaa() {
        return mpaa;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setMpaa(String mpaa) {
        this.mpaa = mpaa;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.dvdid;
        hash = 37 * hash + Objects.hashCode(this.movieTitle);
        hash = 37 * hash + Objects.hashCode(this.mpaa);
        hash = 37 * hash + Objects.hashCode(this.director);
        hash = 37 * hash + Objects.hashCode(this.studio);
        hash = 37 * hash + Objects.hashCode(this.notes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DVD other = (DVD) obj;
        if (this.dvdid != other.dvdid) {
            return false;
        }
        if (!Objects.equals(this.movieTitle, other.movieTitle)) {
            return false;
        }
        if (!Objects.equals(this.mpaa, other.mpaa)) {
            return false;
        }
        if (!Objects.equals(this.director, other.director)) {
            return false;
        }
        if (!Objects.equals(this.studio, other.studio)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        return true;
    }
}

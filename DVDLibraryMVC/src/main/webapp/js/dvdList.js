
$(document).ready(function () {
    loadDVDs();
});
$('#add-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'dvd',
        data: JSON.stringify({
            movieTitle: $('#add-movie-title').val(),
            mpaa: $('#add-mpaa').val(),
            releaseDate: $('#add-release-date').val(),
            director: $('#add-director').val(),
            studio: $('#add-studio').val(),
            notes: $('#add-notes').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#add-movie-title').val('');
        $('#add-mpaa').val('');
        $('#add-release-date').val('');
        $('#add-director').val('');
        $('#add-studio').val('');
        $('#add-notes').val('');
        $('#validationErrors').empty();
        loadDVDs();
    }).error(function (data, status) {
        $.each(data.responseJSON.fieldErrors, function (index, validationError) {
            var errorDiv = $('#validationErrors');
            errorDiv.append(validationError.message).append($('<br>'));
        });
    });
});
$('#edit-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'PUT',
        url: 'dvd/' + $('#edit-dvd-id').val(),
        data: JSON.stringify({
            dvdid: $('#edit-dvd-id').val(),
            movieTitle: $('#edit-movie-title').val(),
            mpaa: $('#edit-mpaa').val(),
            releaseDate: $('#edit-release-date').val(),
            director: $('#edit-director').val(),
            studio: $('#edit-studio').val(),
            notes: $('#edit-notes').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function () {
        loadDVDs();
    });
});

$('#search-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/dvds',
        data: JSON.stringify({
            movieTitle: $('#search-movie-title').val(),
            mpaa: $('#search-mpaa').val(),
            releaseDate: $('#search-release-date').val(),
            director: $('#search-director').val(),
            studio: $('#search-studio').val(),
            notes: $('#search-notes').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#search-movie-title').val('');
        $('#search-mpaa').val('');
        $('#search-release-date').val('');
        $('#search-director').val('');
        $('#search-studio').val('');
        $('#search-notes').val('');
        fillDVDTable(data, status);
    });
});

function loadDVDs() {
    $.ajax({
        url: "dvds"
    }).success(function (data, status) {
        fillDVDTable(data, status);
    });

}

function fillDVDTable(dvdList, status) {
    clearDVDTable();
    var cTable = $('#contentRows');

        $.each(dvdList, function (index, dvd) {
            cTable.append($('<tr>')
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'data-dvd-id': dvd.dvdid,
                                        'data-toggle': 'modal',
                                        'data-target': '#detailsModal'
                                    }).text(dvd.movieTitle)
                                    )
                            )

                    .append($('<td>').text(dvd.mpaa))
                    .append($('<td>').text(dvd.releaseDate))
                    .append($('<td>').text(dvd.director))
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'data-dvd-id': dvd.dvdid,
                                        'data-toggle': 'modal',
                                        'data-target': '#editModal'

                                    }).text('Edit')
                                    )
                            )
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick'
                                                : 'deleteDVD(' + dvd.dvdid + ')'
                                    })
                                    .text('Delete')
                                    )
                            )
                    );

        });
    }

function clearDVDTable() {
    $('#contentRows').empty();
}
$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdid = element.data('dvd-id');
    var modal = $(this);
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdid
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.dvdid);
        modal.find('#dvd-movie-title').text(dvd.movieTitle);
        modal.find('#dvd-mpaa').text(dvd.mpaa);
        modal.find('#dvd-release-date').text(dvd.releaseDate);
        modal.find('#dvd-director').text(dvd.director);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-notes').text(dvd.notes);
    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdid = element.data('dvd-id');
    var modal = $(this);
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdid
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.dvdid);
        modal.find('#edit-dvd-id').val(dvd.dvdid);
        modal.find('#edit-movie-title').val(dvd.movieTitle);
        modal.find('#edit-mpaa').val(dvd.mpaa);
        modal.find('#edit-release-date').val(dvd.releaseDate);
        modal.find('#edit-director').val(dvd.director);
        modal.find('#edit-studio').val(dvd.studio);
        modal.find('#edit-notes').val(dvd.notes);
    });
});
function deleteDVD(id) {
    var answer = confirm("Do you really want to delete this DVD?");

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            loadDVDs();
        });
    }
}
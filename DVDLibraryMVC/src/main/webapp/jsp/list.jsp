<%-- 
    Document   : list
    Created on : Oct 19, 2015, 2:07:20 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/list">List All DVDs</a>
                    </li>

                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row col-md-offset-2">
            <div class="col-md-11"><h2 class="col-md-offset-">My DVDs</h2><br>
                <table id="dvdTable" class="table table-hover">
                    <tr>
                        <th width="20%">Movie Title</th>
                        <th width="15%">MPAA</th>
                        <th width="15%">Release Date</th>
                        <th width="20%">Director</th>
                        <th width="15%"></th>
                        <th width="15%"></th>
                    </tr>
                    <tbody id="contentRows"></tbody>
                </table>

            </div>
        </div>
                     
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                       
                        <h4 class="modal-title" id="detailsModalLabel">DVD Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Movie Title:</th>
                                <td id="dvd-movie-title"></td>
                            </tr>
                            <tr>
                                <th>MPAA:</th>
                                <td id="dvd-mpaa"></td>
                            </tr>
                            <tr>
                                <th>Release Date:</th>
                                <td id="dvd-release-date"></td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td id="dvd-director"></td>
                            </tr>
                            <tr>
                                <th>Studio:</th>
                                <td id="dvd-studio"></td>
                            </tr>
                            <tr>
                                <th>Notes:</th>
                                <td id="dvd-notes"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
              <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="detailsModalLabel">Edit
                            DVD</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-movie-title" class="col-md-4 control-
                                       label">
                                    Movie Title:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-movie-title"
                                           placeholder="Movie Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-mpaa" class="col-md-4 control-
                                       label">
                                    MPAA:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-mpaa"
                                           placeholder="MPAA">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-release-date" class="col-md-4 control-
                                       label">
                                    Release Date:
                                </label>
                                <div class="col-md-8">
                                    <input type="date" class="form-control" id="edit-release-date"
                                           placeholder="Release Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-director" class="col-md-4 control-
                                       label">
                                    Director:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-director"
                                           placeholder="Director">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio" class="col-md-4 control-
                                       label">
                                    Studio:
                                </label>
                                <div class="col-md-8">
                                    <input type="studio" class="form-control" id="edit-studio"
                                           placeholder="Studio">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-notes" class="col-md-4 control-
                                       label">
                                    Notes:
                                </label>
                                <div class="col-md-8">
                                    <input type="tel" class="form-control" id="edit-notes"
                                           placeholder="Notes">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" id="edit-button" class="btn
                                            btn-default"
                                            data-dismiss="modal">
                                        Edit DVD
                                    </button>
                                    <button type="button" class="btn btn-default"
                                            data-dismiss="modal">
                                        Cancel
                                    </button>
                                    <input type="hidden" id="edit-dvd-id">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>      
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
    </body>
</html>
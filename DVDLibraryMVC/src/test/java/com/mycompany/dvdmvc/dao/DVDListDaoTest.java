/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdmvc.dao;

import com.mycompany.dvdmvc.model.DVD;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class DVDListDaoTest {
  
   private DVDListDao dao;

    public DVDListDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (DVDListDao) ctx.getBean("dvdListDao");
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from dvds");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetDeleteDVD() {

        DVD nc = new DVD();
        nc.setMovieTitle("ET");
        nc.setMpaa("PG");
        nc.setReleaseDate("06/11/1982");
        nc.setDirector("Spielberg");
        nc.setStudio("universal");
        nc.setNotes("Classic");
        dao.addDVD(nc);
        DVD fromDb = dao.getdvdid(nc.getDvdid());
        assertEquals(fromDb, nc);
        dao.removeDVD(nc.getDvdid());
        assertNull(dao.getdvdid(nc.getDvdid()));
    }

    @Test
    public void addUpdateDVD() {
        
        DVD nc = new DVD();
        nc.setMovieTitle("Jaws");
        nc.setMpaa("R");
        nc.setReleaseDate("01/01/1975");
        nc.setDirector("Spielberg");
        nc.setStudio("MGM");
        nc.setNotes("ok");
        dao.addDVD(nc);
        DVD fromDb = dao.getdvdid(nc.getDvdid());
        assertEquals(fromDb, nc);
    }

    @Test
    public void getAllDVDses() {

        DVD nc = new DVD();
        nc.setMovieTitle("Jaws");
        nc.setMpaa("R");
        nc.setReleaseDate("01/01/1975");
        nc.setDirector("Spielberg");
        nc.setStudio("MGM");
        nc.setNotes("ok");
        dao.addDVD(nc);

        DVD nc2 = new DVD();
        nc2.setMovieTitle("Dumbo");
        nc2.setMpaa("G");
        nc2.setReleaseDate("01/01/1955");
        nc2.setDirector("Johnson");
        nc2.setStudio("Disney");
        nc2.setNotes("Classic");
        dao.addDVD(nc2);
        List<DVD> cList = dao.getAllDVDs();
        assertEquals(cList.size(), 2);
    }

    @Test
    public void searchDVD() {

        DVD nc = new DVD();
        nc.setMovieTitle("Jaws");
        nc.setMpaa("R");
        nc.setReleaseDate("01/01/1975");
        nc.setDirector("Spielberg");
        nc.setStudio("MGM");
        nc.setNotes("ok");
        dao.addDVD(nc);

        DVD nc2 = new DVD();
        nc2.setMovieTitle("Dumbo");
        nc2.setMpaa("G");
        nc2.setReleaseDate("01/01/1955");
        nc2.setDirector("Johnson");
        nc2.setStudio("Disney");
        nc2.setNotes("Classic");
        dao.addDVD(nc2);

        DVD nc3 = new DVD();
        nc3.setMovieTitle("Avatar");
        nc3.setMpaa("R");
        nc3.setReleaseDate("07/01/2010");
        nc3.setDirector("Lucas");
        nc3.setStudio("Fox");
        nc3.setNotes("Great");
        dao.addDVD(nc3);

        Map<SearchTerm, String> criteria = new HashMap<>();
        criteria.put(SearchTerm.MPAA, "G");
        List<DVD> cList = dao.searchDVD(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc2, cList.get(0));

        criteria.put(SearchTerm.MPAA, "R");
        cList = dao.searchDVD(criteria);
        assertEquals(2, cList.size());

        criteria.put(SearchTerm.DIRECTOR, "Spielberg");
        cList = dao.searchDVD(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc, cList.get(0));

        criteria.put(SearchTerm.DIRECTOR, "Lucas");
        cList = dao.searchDVD(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc3, cList.get(0));

        criteria.put(SearchTerm.DIRECTOR, "Johnson");
        cList = dao.searchDVD(criteria);
        assertEquals(0, cList.size());
        criteria.put(SearchTerm.DIRECTOR, "Johnson");
        cList = dao.searchDVD(criteria);
        assertEquals(0, cList.size());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdmvc.controller;

import com.mycompany.dvdmvc.dao.DVDListDao;
import com.mycompany.dvdmvc.model.DVD;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
/**
 *
 * @author apprentice
 */
public class ListController {
 private DVDListDao dao;
    
    @Inject
    public ListController (DVDListDao dao) {
        this.dao=dao;
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String displayListPage(){
        return ("list");
    }
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.GET)
    @ResponseBody public DVD getDVD (@PathVariable ("id")int id) {
        return dao.getDVDId(id);
    }
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.DELETE)
    @ResponseStatus (HttpStatus.NO_CONTENT)
    public void deleteDVD (@PathVariable("id") int id){
        dao.removeDVD(id);
    }
    @RequestMapping (value="/dvd/{id}",method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putAddress (@PathVariable ("id")int id, @RequestBody DVD dvd) {
        dvd.setDVDId(id);
        dao.updateDVD(dvd);
    }
    @RequestMapping(value="/dvds",method=RequestMethod.GET)
    @ResponseBody public List<DVD> getAllAddresses(){
        return dao.getAllDVDs();
    }
    
    
}

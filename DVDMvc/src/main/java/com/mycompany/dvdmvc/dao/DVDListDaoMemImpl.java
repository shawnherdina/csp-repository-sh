/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdmvc.dao;

import com.mycompany.dvdmvc.model.DVD;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDListDaoMemImpl implements DVDListDao {
    private Map<Integer, DVD> dvdMap = new HashMap<>();

    private static int dvdIdCounter = 0;

    @Override
    public DVD addDVD(DVD dvd) {
        dvd.setDVDId(dvdIdCounter);
        dvdIdCounter++;
        dvdMap.put(dvd.getDVDId(), dvd);
        return dvd;
    }

    @Override
    public void removeDVD(int dvdId) {
        dvdMap.remove(dvdId);
    }

    @Override
    public void updateDVD(DVD dvd) {
        dvdMap.put(dvd.getDVDId(), dvd);
    }

    @Override
    public List<DVD> getAllDVDs() {
        Collection<DVD> c = dvdMap.values();
        return new ArrayList(c);
    }
    @Override
    public DVD getDVDId(int dvdId) {
        return dvdMap.get(dvdId);
    }

    @Override
    public List<DVD> searchDVD(Map<SearchTerm, String> criteria) {
        String movieTitleCriteria = criteria.get(SearchTerm.MOVIE_TITLE);
        String mpaaCriteria = criteria.get(SearchTerm.MPAA);
        String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);
        String notesCriteria = criteria.get(SearchTerm.NOTES);

        Predicate<DVD> movieTitleMatches;
        Predicate<DVD> mpaaMatches;
        Predicate<DVD> directorMatches;
        Predicate<DVD> studioMatches;
        Predicate<DVD> notesMatches;

        Predicate<DVD> truePredicate = (c) -> {
            return true;
        };
        movieTitleMatches = (movieTitleCriteria == null || movieTitleCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getMovieTitle().equals(movieTitleCriteria);
        mpaaMatches = (mpaaCriteria == null || mpaaCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getMpaa().equals(mpaaCriteria);
        directorMatches = (directorCriteria == null || directorCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getDirector().equals(directorCriteria);
        studioMatches = (studioCriteria == null || studioCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getStudio().equals(studioCriteria);
        notesMatches = (notesCriteria == null || notesCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getNotes().equals(notesCriteria);
        return dvdMap.values().stream()
                .filter(movieTitleMatches
                        .and(mpaaMatches)
                        .and(directorMatches)
                        .and(studioMatches)
                        .and(notesMatches))
                .collect(Collectors.toList());
    }

}

    



$(document).ready(function () {
    loadDVDs();
})
$('#add-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'dvd',
        data: JSON.stringify({
            movieTitle: $('#add-movie-title').val(),
            mpaa: $('#add-mpaa').val(),
            releaseDate:$('#add-release-date').val(),
            director: $('#add-director').val(),
            studio: $('#add-studio').val(),
            notes: $('#add-notes').val(),
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#add-movie-title').val('');
        $('#add-mpaa').val('');
        $('#add-release-date').val('');
        $('#add-director').val('');
        $('#add-studio').val('');
        $('#add-notes').val('');
        loadDVDs();
    });
});
$('#edit-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'PUT',
        url: 'dvd/' + $('#edit-dvd-id').val(),
        data: JSON.stringify({
            dvdId: $('#edit-dvd-id').val(),
            movieTitle: $('#edit-movie-title').val(),
            mpaa: $('#edit-mpaa').val(),
            releaseDate:$('#edit-releaseDate').val(),
            director: $('#edit-director').val(),
            studio: $('#edit-studio').val(),
            notes: $('#edit-notes').val(),
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function () {
        loadDVDs();
    });
});
function loadDVDs() {
    clearDVDTable();
    var cTable = $('#contentRows');
    $.ajax({
        url: "DVDs"
    }).success(function (data, status) {

        $.each(data, function (index, address) {
            cTable.append($('<tr>')
                    .append($('<td>').text(dvd.movieTitle))
                    .append($('<td>').text(dvd.mpaa))
                    .append($('<td>').text(dvd.releaseDate))
                    .append($('<td>').text(dvd.director))
                    .append($('<td>')
                            .append($('<a>').attr({
                                'data-address-id': dvd.dvdId,
                                'data-toggle': 'modal',
                                'data-target': '#displayModal'
                            }).text('Edit'))
                            )
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'deleteDVD(' + dvd.dvdId + ')'
                                    })
                                    .text('Delete')
                                    )
                            )
                    );
        });
    });
}

function clearDVDTable() {
    $('#dvdRows').empty();
}
$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    modal.find('#dvd-id').text(dvd.dvdId);
    modal.find('#dvd-movieTitle').text(dvd.movieTitle);
    modal.find('#dvd-mpaa').text(dvd.mpaa);
    modal.find('#dvd-releaseDate').text(dvd.releaseDate);
    modal.find('#dvd-director').text(dvd.director);
    modal.find('#dvd-studio').text(dvd.studio);
    modal.find('#dvd-notes').text(dvd.notes);
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    modal.find('#dvd-id').text(dvd.dvdId);
    modal.find('#edit-movieTitle').val(dvd.movieTitle);
    modal.find('#edit-mpaa  ').val(dvd.mpaa);
    modal.find('#edit-releaseDate').val(dvd.releaseDate);
    modal.find('#edit-director').val(dvd.director);
    modal.find('#edit-studio').val(dvd.studio);
    modal.find('#edit-notes').val(dvd.notes);
});
//var testDVDData = [
//    {
//        dvdId: 1,
//        movieTitle: "ET",
//        mpaa: "PG",
//        releaseDate: "1/1/87",
//        director: "Spielberg",
//        studio: "MGM",
//        userRating: "4 Stars"},
//    {
//        dvdId: 2,
//        movieTitle: "Die Hard",
//        mpaa: "R",
//        releaseDate: "1/1/87",
//        director: "Bad Ass McGee",
//        studio: "Fox",
//        userRating: "5 Stars"},
//    {
//        dvdId: 3,
//        movieTitle: "Die Hard 2",
//        mpaa: "R",
//        releaseDate: "1/1/88",
//        director: "Bad Ass Jr",
//        studio: "Fox",
//        userRating: "4 Stars"}
//];
//var dvd =
//        {
//            dvdId: 42,
//            movieTitle: "ET",
//            mpaa: "PG",
//            releaseDate: "1/1/87",
//            director: "Spielberg",
//            studio: "MGM",
//            userRating: "4 Stars"};
//
//var dvd =
//        {
//            dvdId: 52,
//            movieTitle: "ET",
//            mpaa: "PG",
//            releaseDate: "1/1/87",
//            director: "Spielberg",
//            studio: "MGM",
//            userRating: "4 Stars"}

<%-- 
    Document   : home
    Created on : Oct 22, 2015, 8:49:36 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/list">List All DVDs</a>
                    </li>

                    <li>
                        <div class="dropdown">
                            <button class="btn btn-default btn-md dopdown-toggle" type="button" data-toggle="dropdown">Search
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>By MPAA</li>
                                <li>By Release Date</li>
                                <li>By Studio</li>
                                <li>By Director</li>
                                <li>By Title</li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 ">
                <h2 class ="col-md-offset-4">Add New DVD</h2><br>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="add-movie-title" class="col-md-4 control-label">
                            Movie Title:
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-movie-title" placeholder="Movie Title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-mpaa" class="col-md-4 control-label">
                            MPAA:
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-mpaa" placeholder="Rating"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-release-date" class="col-md-4 control-label">
                            Release Date:
                        </label>
                        <div class="col-md-8">
                            <input type="calendar" class="form-control" id="add-release-date" placeholder="Release Date"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-director" class="col-md-4 control-label">
                            Director:
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-director" placeholder="Director"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-studio" class="col-md-4 control-label">
                            Studio:
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-studio" placeholder="Studio"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-notes" class="col-md-4 control-label">
                            Notes:
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-notes" placeholder="Notes"/>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-md-offset-4 clo-md-8">
                            <button type="submit" id="add-button" class="btn btn-default">
                                Create DVD:
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
    </body>
</html>
<%-- 
    Document   : list
    Created on : Oct 22, 2015, 8:50:09 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/list">List All DVDs</a>
                    </li>

                    <li>
                        <div class="dropdown">
                            <button class="btn btn-default btn-md dopdown-toggle" type="button" data-toggle="dropdown">Search
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>By MPAA</li>
                                <li>By Release Date</li>
                                <li>By Studio</li>
                                <li>By Director</li>
                                <li>By Title</li>
                            </ul>
                        </div>
                    </li>


                </ul>
            </div>
        </div>
        <div class="row col-md-offset-2">
            <div class="col-md-6"><h2 class="col-md-offset-">My DVDs</h2><br>
                <table id="dvdTable" class="table table-hover">
                    <tr>
                        <th width="20%">Movie Title</th>
                        <th width="10%">MPAA</th>
                        <th width="15%">Release Date</th>
                        <th width="20%">Director</th>
                        <th width="15%">Studio</th>
                        <th width="10%">Notes</th>
                        <th width="10%"></th>
                    </tr>
                    <tbody id="contentRows"></tbody>
                </table>

            </div>
        </div>
                     
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="detailsModalLabel">DVD
                            Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Movie Title:</th>
                                <td id="dvd-firstName"></td>
                            </tr>
                            <tr>
                                <th>MPAA:</th>
                                <td id="dvd-lastName"></td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td id="dvd-director"></td>
                            </tr>
                            <tr>
                                <th>Phone:</th>
                                <td id="dvd-notes"></td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td id="dvd-studio"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="detailsModalLabel">DVD
                            Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Movie Title:</th>
                                <td id="dvd-movieTitle"></td>
                            </tr>
                            <tr>
                                <th>MPAA:</th>
                                <td id="dvd-mpaa"></td>
                            </tr>
                            <tr>
                                <th>Release Date:</th>
                                <td id="dvd-releaseDate"></td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td id="dvd-director"></td>
                            </tr>
                            <tr>
                                <th>Studio:</th>
                                <td id="dvd-studio"></td>
                            </tr>
                            <tr>
                                <th>Notes:</th>
                                <td id="dvd-notes"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
    </body>
</html>
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdmvc;

import com.mycompany.dvdmvc.dao.DVDListDao;
import com.mycompany.dvdmvc.dao.SearchTerm;
import com.mycompany.dvdmvc.model.DVD;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DVDListDaoTest1 {
  
    private DVDListDao dao;

    public DVDListDaoTest1() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("dvdListDao", DVDListDao.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetDeleteDVD() {
// create new address
        DVD nc = new DVD();
        nc.setMovieTitle("John");
        nc.setMpaa("Doe");
        nc.setDirector("Oracle");
        nc.setStudio("john@doe.com");
        nc.setNotes("1234445678");
        dao.addDVD(nc);
        DVD fromDb = dao.getDVDId(nc.getDVDId());
        assertEquals(fromDb, nc);
        dao.removeDVD(nc.getDVDId());
        assertNull(dao.getDVDId(nc.getDVDId()));
    }

    @Test
    public void addUpdateDVD() {
        // create new address
        DVD nc = new DVD();
        nc.setMovieTitle("Jimmy");
        nc.setMpaa("Smith");
        nc.setDirector("Sun");
        nc.setStudio("jimmy@smith.com");
        nc.setNotes("1112223333");
        dao.addDVD(nc);
        nc.setNotes("9999999999");
        dao.updateDVD(nc);
        DVD fromDb = dao.getDVDId(nc.getDVDId());
        assertEquals(fromDb, nc);
    }

    @Test
    public void getAllDVDses() {
// create new address
        DVD nc = new DVD();
        nc.setMovieTitle("Jimmy");
        nc.setMpaa("Smith");
        nc.setDirector("Sun");
        nc.setStudio("jimmy@smith.com");
        nc.setNotes("1112223333");
        dao.addDVD(nc);
// create new address
        DVD nc2 = new DVD();
        nc2.setMovieTitle("John");
        nc2.setMpaa("Jones");
        nc2.setDirector("Apple");
        nc2.setStudio("john@jones.com");
        nc2.setNotes("5556667777");
        dao.addDVD(nc2);
        List<DVD> cList = dao.getAllDVDs();
        assertEquals(cList.size(), 2);
    }

    @Test
    public void searchDVD() {
// create new address
        DVD nc = new DVD();
        nc.setMovieTitle("Jimmy");
        nc.setMpaa("Smith");
        nc.setDirector("Sun");
        nc.setStudio("jimmy@smith.com");
        nc.setNotes("1112223333");
        dao.addDVD(nc);
// create new address
        DVD nc2 = new DVD();
        nc2.setMovieTitle("John");
        nc2.setMpaa("Jones");
        nc2.setDirector("Apple");
        nc2.setStudio("john@jones.com");
        nc2.setNotes("5556667777");
        dao.addDVD(nc2);
// create new address - same last name as first address but different
// address
        DVD nc3 = new DVD();
        nc3.setMovieTitle("Steve");
        nc3.setMpaa("Smith");
        nc3.setDirector("Microsoft");
        nc3.setStudio("steve@msft.com");
        nc3.setNotes("5552221234");
        dao.addDVD(nc3);
// Create search criteria
        Map<SearchTerm, String> criteria = new HashMap<>();
        criteria.put(SearchTerm.MPAA, "Jones");
        List<DVD> cList = dao.searchDVD(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc2, cList.get(0));
// New search criteria - look for Smith
        criteria.put(SearchTerm.MPAA, "Smith");
        cList = dao.searchDVD(criteria);
        assertEquals(2, cList.size());
// Add address to search criteria
        criteria.put(SearchTerm.DIRECTOR, "Sun");
        cList = dao.searchDVD(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc, cList.get(0));
// Change address to Microsoft, should get back nc3
        criteria.put(SearchTerm.DIRECTOR, "Microsoft");
        cList = dao.searchDVD(criteria);
        assertEquals(1, cList.size());
        assertEquals(nc3, cList.get(0));
// Change address to Apple, should get back nothing
        criteria.put(SearchTerm.DIRECTOR, "Apple");
        cList = dao.searchDVD(criteria);
        assertEquals(0, cList.size());
        criteria.put(SearchTerm.DIRECTOR, "Apple");
        cList = dao.searchDVD(criteria);
        assertEquals(0, cList.size());
    }
}

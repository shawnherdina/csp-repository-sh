/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.firstmavenproject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Olympians {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
//        SkiJumper sj = (SkiJumper) ctx.getBean("superSkiJumper");
//        System.out.println(sj.competeInEvent());
//
//        Event skiJumpEvent = new SkiJumpEvent();
//        Olympian olympianSkiJumper = new Olympian(skiJumpEvent);
//        olympianSkiJumper.competeInEvent();
        Scanner kb = new Scanner(System.in);
        String choice;
        System.out.print("Choose an Olympian: ");
        choice = kb.next();

        System.out.println(choice);
        Olympian canadaSpeedSkater = (Olympian) ctx.getBean("canadianSpeedSkater");
        canadaSpeedSkater.competeInEvent();

//        Olympian usaSkiJumper = (Olympian) ctx.getBean("usaSkiJumper");
//        usaSkiJumper.competeInEvent();
//        
//        Olympian usaSpeedSkater = (Olympian) ctx.getBean("usaSpeedSkater");
//        usaSpeedSkater.competeInEvent();
//        
//        Olympian canadaSpeedSkater = (Olympian) ctx.getBean("canadianSpeedSkater");
//        canadaSpeedSkater.competeInEvent();
//    }
    }
}

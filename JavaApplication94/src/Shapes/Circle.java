/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shapes;

/**
 *
 * @author apprentice
 */
public class Circle extends Shapes {

    String color;
    double radius;
    double area;
    double perimeter;

    public Circle(String color, double radius, double area, double perimeter) {
        super(color);
        this.radius = radius;
        this.area = area;
        this.perimeter = perimeter;
    }

    @Override
    public double getArea() {
        double area = 0;
        area = Math.PI * (radius * radius);

        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 0;

        perimeter = (2 * Math.PI) * radius;

        return perimeter;
    }
}

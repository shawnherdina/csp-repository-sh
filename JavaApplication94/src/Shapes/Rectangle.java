/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shapes;

/**
 *
 * @author apprentice
 */
public class Rectangle extends Shapes {

    private String color;
    private double perimeter;
    private double area;
    private double length;
    private double width;

    public Rectangle(String color, double perimeter, double area, double length, double width) {
        super(color);
        this.perimeter = perimeter;
        this.area = area;
        this.length = length;
        this.width = width;
    }

    public double getArea() {

        return length * width;

    }

    public double getPerimeter() {

        return (length * 2) + (width * 2);
    }
}

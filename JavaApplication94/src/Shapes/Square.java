/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shapes;

/**
 *
 * @author apprentice
 */
public class Square extends Shapes {

    String color;
    double side;

    public Square(String color, double side) {
        super(color);
        this.side = side;

    }

    @Override

    public double getArea() {

        return side * side;
    }

    @Override

    public double getPerimeter() {

        return side * 4;
    }
}

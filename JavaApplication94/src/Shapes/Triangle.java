/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shapes;

/**
 *
 * @author apprentice
 */
public class Triangle extends Shapes {

    private String color;
    private double side1;
    private double side2;
    private double side3;
    private double area;
    private double perimeter;

    public Triangle(String color, double side1, double side2, double side3, double area, double perimeter) {
        super(color);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.area = area;
        this.perimeter = perimeter;
    }

    public double getArea() {
        double s = (side1 + side2 + side3) / 2;
        return Math.sqrt(s * ((s - side1) * (s - side2) * (s - side3)));
    }

    public double getPerimeter() {
        return side1 + side2 + side3;
    }
}

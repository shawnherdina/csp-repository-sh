
public class Logic {

    public boolean GreatParty(int cigars, boolean isWeekend) {
        if (isWeekend) {
            if (cigars >= 40) {
                return true;
            } else {
                return false;
            }
        } else {
            if (cigars >= 40 && cigars <= 60) {
                return true;
            } else {
                return false;
            }
        }
    }

    public int CanHazTable(int yourStyle, int dateStyle) {
        int no = 0, maybe = 1, yes = 2;
        if ((yourStyle >= 8 || dateStyle >= 8) && (yourStyle > 2 || dateStyle > 2)) {
            return yes;
        } else if ((yourStyle > 8 || dateStyle > 8) && (yourStyle <= 2 || dateStyle <= 2)) {
            return maybe;
        } else {
            return no;
        }
    }

    public boolean PlayOutside(int temp, boolean isSummer) {

        if (isSummer) {
            if (temp >= 60 && temp <= 100) {
                return true;
            } else {
                return false;
            }
        } else {
            if (temp >= 60 && temp <= 90) {
                return true;
            } else {
                return false;
            }
        }
    }

    public int CaughtSpeeding(int speed, boolean isBirthday) {
        int noTicket = 0, smallTicket = 1, bigTicket = 2;
        if (speed >= 60) {
            return noTicket;

        } else if ((speed >= 61 && speed <= 80)) {
            return smallTicket;
        } else {
            return bigTicket;
        }
    }

    public int SkipSum(int a, int b) {
        int sum;
        sum = a + b;
        if (sum >= 10 && sum <= 10) {
            return 20;
        } else {
            return sum;
        }
    }

    public String AlarmClock(int day, boolean vacation) {
        if (vacation) {
            if (day >= 1 || day <= 5) {
                return "10:00";
            } else {
                return "off";
            }
        } else {
            if (day >= 1 || day <= 5) {
                return "7:00";
            } else {
                return "10:00";
            }
        }
    }

    public boolean LoveSix(int a, int b) {
        int sum = 0;
        if (sum == 6 || a == 6 || b == 6) {
            return true;
        } else {
            return false;
        }
    }

    public boolean InRange(int n, boolean outsideMode) {
        if ((n >= 1 || n <= 10) && (outsideMode = false)) {
            return true;
        } else if (outsideMode == true) {
            return true;
        }else
                return false;
        }
    }


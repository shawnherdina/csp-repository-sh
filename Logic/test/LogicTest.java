/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LogicTest {
    
    public LogicTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of GreatParty method, of class Logic.
     */
    @Test
    public void testGreatParty() {
        System.out.println("GreatParty");
        int cigars = 0;
        boolean isWeekend = false;
        Logic instance = new Logic();
        boolean expResult = false;
        boolean result = instance.GreatParty(30, false);
        assertEquals(false, result);
        result = instance.GreatParty(50,false);
        assertEquals(true, result);
        result = instance.GreatParty(70,true);
        assertEquals(true, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of CanHazTable method, of class Logic.
     */
    @Test
    public void testCanHazTable() {
        System.out.println("CanHazTable");
        int yourStyle = 0;
        int dateStyle = 0;
        Logic instance = new Logic();
        int expResult = 0;
        int result = instance.CanHazTable(5,10);
        assertEquals(2, result);
        result = instance.CanHazTable(5,2);
        assertEquals(0, result);
        result = instance.CanHazTable(5,5);
        assertEquals(1, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of PlayOutside method, of class Logic.
     */
    @Test
    public void testPlayOutside() {
        System.out.println("PlayOutside");
        int temp = 0;
        boolean isSummer = false;
        Logic instance = new Logic();
        boolean expResult = false;
        boolean result = instance.PlayOutside(70,false);
        assertEquals(true, result);
        result = instance.PlayOutside(95,false);
        assertEquals(false, result);
        result = instance.PlayOutside(95,true);
        assertEquals(true, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of CaughtSpeeding method, of class Logic.
     */
    @Test
    public void testCaughtSpeeding() {
        System.out.println("CaughtSpeeding");
        int speed = 0;
        boolean isBirthday = false;
        Logic instance = new Logic();
        int expResult = 0;
        int result = instance.CaughtSpeeding(60,false);
        assertEquals(0, result);
        result = instance.CaughtSpeeding(65,false);
        assertEquals(1, result);
        result = instance.CaughtSpeeding(65,true);
        assertEquals(0, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of SkipSum method, of class Logic.
     */
    @Test
    public void testSkipSum() {
        System.out.println("SkipSum");
        int a = 0;
        int b = 0;
        Logic instance = new Logic();
        int expResult = 0;
        int result = instance.SkipSum(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of AlarmClock method, of class Logic.
     */
    @Test
    public void testAlarmClock() {
        System.out.println("AlarmClock");
        int day = 0;
        boolean vacation = false;
        Logic instance = new Logic();
        String expResult = "";
        String result = instance.AlarmClock(day, vacation);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of LoveSix method, of class Logic.
     */
    @Test
    public void testLoveSix() {
        System.out.println("LoveSix");
        int a = 0;
        int b = 0;
        Logic instance = new Logic();
        boolean expResult = false;
        boolean result = instance.LoveSix(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of InRange method, of class Logic.
     */
    @Test
    public void testInRange() {
        System.out.println("InRange");
        int n = 0;
        boolean outsideMode = false;
        Logic instance = new Logic();
        boolean expResult = false;
        boolean result = instance.InRange(n, outsideMode);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}

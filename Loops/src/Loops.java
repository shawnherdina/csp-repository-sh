
public class Loops {

    public String StringTimes(String str, int n) {
        String s = "";
        for (int i = 0; i < n; i++) {
            s = s + str;
        }
        return s;

    }

    public String FrontTimes(String str, int n) {

        String s = "";

        for (int i = 0; i < n; i++) {
            s = s + str;
        }
        return s;
    }

    public int CountXX(String str) {
        int count = 0;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.substring(i, i + 2).equals("xx")) {
                count++;
            }
        }
        return count;
    }

    public boolean DoubleX(String str) {
        int i = str.indexOf('x');

        if (i == -1 || i == str.length() - 1) {
            return false;
        }

        return str.substring(i, i + 2).equals("xx");

    }

    public String EveryOther(String str) {
        String result = "";

        for (int i = 0; i < str.length(); i += 2) {
            result = result + str.charAt(i);
        }

        return result;

    }

    public String StringSplosion(String str) {
        String result = "";

        for (int i = 0; i < str.length(); i++) {
            result += str.substring(0, i + 1);
        }
        return result;
    }

    public int CountLast2(String str) {
        if (str.length() < 2) {
            return 0;
        }

        String end = str.substring(str.length() - 2);
        int count = 0;

        for (int i = 0; i < str.length() - 2; i++) {
            String sub = str.substring(i, i + 2);

            if (sub.equals(end)) {
                count++;
            }
        }

        return count;
    }

    public int Count9(int[] numbers) {
        int count = 0;

        for (int num : numbers) {
            if (num == 9) {
                count++;
            }
        }

        return count;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        int die1, die2, dice, countAtMax = 0, RemMoney = 0, MaxMoney = 0, count = 0;

        System.out.print("How many dollars do you have to play? ");
        RemMoney = kb.nextInt();

        while (RemMoney >=0) {
            count++;
            die1 = (int) (Math.random() * 6) + 1;
            die2 = (int) (Math.random() * 6) + 1;

            dice = die1 + die2;
            
            //debugging
            //System.out.println(die1+" "+die2+" "+dice+" "+countAtMax+" "+RemMoney+" "+MaxMoney+" "+count+" ");

            if (dice == 7) {
                RemMoney += 4;
            } else {
                RemMoney -= 1;
            }
            if (RemMoney > MaxMoney) {
                MaxMoney = RemMoney;
                countAtMax = count;
            }
        }
        System.out.println("You are broke after " + count);
        System.out.println("You should have quit after " + countAtMax + " when you had " + MaxMoney);

    }

    // TODO code application logic here
}

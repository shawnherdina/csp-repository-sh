
import java.util.Scanner;
import java.util.Random;

public class MGRockPaperScissors {

    public static Scanner kb = new Scanner(System.in);
    public static Random rnd = new Random();

    public static int results(int p2Choice, int p1Choice) {

        if (p1Choice == p2Choice) {
            System.out.println("You tied.");
            return 0;
        } else if ((p2Choice == 3 && p1Choice == 2)
                || (p2Choice == 2 && p1Choice == 1)
                || (p2Choice == 1 && p1Choice == 3)) {
            System.out.println("You lost.");
            return 1;
        } else {
            System.out.println("You won.");
            return 2;
        }

    }

    public static int computerChoice() {
        int compChoice;
        String compString;

        compChoice = rnd.nextInt(3) + 1;

        if (compChoice == 1) {
            compString = "Rock";
        }
        if (compChoice == 2) {
            compString = "Paper";
        }
        if (compChoice == 3) {
            compString = "Scissors";
        }

        return compChoice;
    }

    public static int playerChoice() {
        String p1String;
        int plChoice;

        do {
            System.out.print("Your move:\n"
                    + "1) Rock \n"
                    + "2) Paper \n"
                    + "3) Scissors \n"
                    + ": "
            );
            p1String = kb.nextLine();
            plChoice = Integer.parseInt(p1String);
        } while (plChoice < 1 || plChoice > 3);

        if (plChoice == 1) {
            p1String = "Rock";
        }
        if (plChoice == 2) {
            p1String = "Paper";
        }
        if (plChoice == 3) {
            p1String = "Scissors";
        }

        return plChoice;

    }

    public static void main(String[] args) {

        String p1String, again = "";
        int p1Choice, p2Choice, rounds, ties, wins, losses, whoWon;
        do {
            do {
                System.out.print("How many rounds of Rock-Paper-Scissors would you like to play? (1-10)");
                rounds = kb.nextInt();
                kb.nextLine();
            } while (rounds < 1 || rounds > 10);

            ties = wins = losses = 0;

            for (int i = 0; i < rounds; i++) {

                p1Choice = playerChoice();
                p2Choice = computerChoice();

                System.out.println("You chose " + p1Choice + ", Computer chose " + p2Choice);

                whoWon = results(p2Choice, p1Choice);
                if (whoWon == 0) {
                    ties++;
                }
                if (whoWon == 1) {
                    losses++;
                }
                if (whoWon == 2) {
                    wins++;
                }

                System.out.println("Ties: " + ties);
                System.out.println("User Wins: " + wins);
                System.out.println("Computer wins: " + losses);

                if (wins == losses) {
                    System.out.println("The overall game was a tie.");
                } else if (wins > losses) {
                    System.out.println("The user wins.");
                } else {
                    System.out.println("The computer wins.");
                }

                do {
                    System.out.print("Would you like to play again? (Yes/No) ");
                    again = kb.nextLine();
                    if (!(again.equalsIgnoreCase("Yes") || again.equalsIgnoreCase("No"))) {
                        System.out.println("Unrecognized response");
                    }
                } while (!(again.equalsIgnoreCase("Yes") || again.equalsIgnoreCase("No")));

            }

        } while (again.equalsIgnoreCase("Yes"));
    }
}

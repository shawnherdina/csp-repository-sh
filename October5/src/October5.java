/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class October5 {

    public static void SelectionSort(Comparable[] a) {

    }

    public static void InsertionSort(Comparable[] a) {
        Comparable current;
        for (int i = 1; i < a.length; i++) {
            current = a[i];

            for (int j = i; j >= 0; j--) {
                if (j != 0 && current.compareTo(a[j - 1]) < 0) {
                    a[j] = a[j - 1];

                } else {
                    a[j] = current;
                    break;
                }
            }
        }
    }

    public static void mergeSort(Comparable[] a) {
        Comparable[] sorted = mergeSortHelper(a, 0, a.length - 1);
        for (int i = 0; i < a.length; i++) {
            a[i] = sorted[i];
        }

    }

    public static Comparable[] mergeSortHelper(Comparable[] a, int start, int end) {
        if (start == end) {
            return a;
        }
        Comparable[] temp1 = mergeSortHelper(a, start, (start + end) / 2);
        Comparable[] temp2 = mergeSortHelper(a, (start + end) / 2 + 1, end);
        Comparable[] sorted = new Comparable[a.length];

        int cnt1 = 0, cnt2 = 0, next = 0;
        while (cnt1 < temp1.length && cnt2 < temp2.length) {
            if (temp1[cnt1].compareTo(temp2[cnt2]) < 0) {
                sorted[next] = temp1[cnt1];
                cnt1++;
            } else {
                sorted[next] = temp2[cnt2];
                cnt2++;
            }
            next++;
        }
        if (cnt1 == temp1.length) {
            while (cnt2 < temp2.length) {
                sorted[next] = temp2[cnt2];
                cnt2++;
                next++;
            }
        } else {
            while (cnt2 < temp2.length) {
                sorted[next] = temp2[cnt2];
                cnt2++;
                next++;

            }
        }
        return sorted;
    }

    public static void main(String[] args) {
        String[] arr = {"b", "c", "q", "a", "f", "d"};
        InsertionSort(arr);

        for (String s : arr) {
            System.out.print(s + ' ');

        }
        System.out.println();
    }
}

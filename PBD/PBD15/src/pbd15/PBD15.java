/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd15;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb=new Scanner(System.in);
        String word1,word2;
        int number1,number2;
        
        System.out.print("Give me a word!");
        word1=kb.nextLine();
        System.out.println(word1);
        
        System.out.print("Give me a second word!");
        word2=kb.nextLine();
        System.out.println(word2);
        
        
        System.out.print("Great, now your favorite number? ");
        number1=kb.nextInt();
        System.out.println(number1);
        
        System.out.print("And your second-favorite number...");
        number2=kb.nextInt();
        System.out.println(number2);
        
        
        System.out.println("Whew! Wasn't that fun");
       
        
      
    }
    
}

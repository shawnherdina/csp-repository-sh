/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd16;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD16 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb=new Scanner (System.in);
        String name;
        int age;
        double salary;
        
        System.out.print("Hello, What is your name? ");
        name=kb.nextLine();
        System.out.println(name);
        
        System.out.print("Hi, " + name +"! How old are you?");
        age=kb.nextInt();
        System.out.println(age);
        
        System.out.print("So you're "+age+", eh? That's not old at all! How much do you make, " + name+"?");
        salary=kb.nextDouble();
        System.out.println(salary);
        
        System.out.println(salary + "! I hope that is per hour and not per year. Lol");
        
    }
    
}

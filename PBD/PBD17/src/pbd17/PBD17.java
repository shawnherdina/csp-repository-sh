/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd17;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb=new Scanner (System.in);
        String first,last;
        int Grade, StudentID;
        String login;
        double GPA;
        
        System.out.print("First name: ");
        first=kb.nextLine();
        System.out.print("Last name: ");
        last=kb.nextLine();
        System.out.print("Grade (9-12): ");
        Grade=kb.nextInt();
        System.out.print("Student ID: ");
        StudentID=kb.nextInt();
        System.out.print("Login: ");
        login=kb.nextLine();
        System.out.print("GPA (0.0-4.0)");
        GPA=kb.nextDouble();
        
        System.out.println("Your information: ");
        System.out.println("Login: " +login);
        System.out.println("ID: " + StudentID);
        System.out.println("Name: "+last+","+first);
        System.out.println("GPA: " + GPA);
        System.out.println("Grade: " + Grade);
    }
    
}

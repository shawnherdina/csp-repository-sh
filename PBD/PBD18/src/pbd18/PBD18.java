/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd18;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class PBD18 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb=new Scanner(System.in);
        String name;
        int age;
        
        System.out.print("Hello. What is your name? ");
        name=kb.nextLine();
        
        System.out.print("Hi, "+name+ "!"+"How old are you? ");
        age=kb.nextInt();
        
        System.out.println("Did you know that five years you will be " + (age+5) + " years old?");
        System.out.println("And five years ago you were " +(age-5) + " years old");
    }
    
}

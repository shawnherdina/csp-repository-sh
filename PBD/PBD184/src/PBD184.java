
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class PBD184 {

    public static void main(String[] args) {
        Random rdm = new Random();
        ArrayList<Integer> arr = new ArrayList();
        Scanner kb = new Scanner(System.in);
        int num = 0;

        System.out.print("Pick a number: ");
        num = kb.nextInt();

        System.out.print("ArrayList: ");
        for (int i = 0; arr.size() < 10; i++) {
            arr.add(i, 1 + rdm.nextInt(1 + 100));
        } 
        System.out.println(arr);

        int curr = 0;

        for (int j : arr) {
            if (j == num) {
                curr = j;
            }
        }
        System.out.println("Your number is in the Array and located " + arr.get(num) + " spot!!");
        if (curr != num) {
            System.out.println("Your number is Not in the Array");
        }
    }
}

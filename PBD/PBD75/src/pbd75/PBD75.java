/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd75;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class PBD75 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random random = new Random();
        int card1, card2, dealer1, dealer2;
        int cardTotal, dealerTotal;

        dealer1 = random.nextInt(10) + 1;
        dealer2 = random.nextInt(10) + 1;
        dealerTotal = dealer1 + dealer2;

        card1 = random.nextInt(10) + 1;
        card2 = random.nextInt(10) + 1;
        cardTotal = card1 + card2;

        System.out.println("You have: " + card1 + " , " + card2 + " = " + cardTotal);
        System.out.println("Dealer has: " + dealer1 + " , " + dealer2 + " = " + dealerTotal);
        if (cardTotal > dealerTotal) {
            System.out.println("You win!!");
        } else {
            System.out.println("You lose");

        }
        if (cardTotal == dealerTotal) {
            System.out.println("You pushed");

        }
    }
}

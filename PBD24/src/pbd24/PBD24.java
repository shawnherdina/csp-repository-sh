/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd24;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD24 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int day;

        System.out.print("Enter a number for day of the week: ");
        day = kb.nextInt();
        
        if (day==1){
            System.out.println("Today is Sunday");
        }
        else if (day==2){
        System.out.println("Today is Monday");
    }
        else if (day==3){
        System.out.println("Today is Tuesday");
    }
        else if (day==4){
        System.out.println("Today is Wednesday");
    }
        else if (day==5){
        System.out.println("Today is Thursday");
    }
        else if (day==6){
        System.out.println("Today is Friday");
    }
        else if (day==7){
        System.out.println("Today is Saturday");
    }
        else{
            System.out.println("You entered an invalid day");
        }
    }

}

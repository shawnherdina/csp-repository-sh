/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd26;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD26 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        double weight;
        int choice;

        System.out.print("Please enter your earth weight: ");
        weight = kb.nextDouble();

        System.out.println("I have information for the following planets: " + "\n"
                + "1.Venus"+"\n"
                + "2.Mars"+"\n"
                + "3.Jupiter"+"\n"
                + "4.Saturn"+"\n"
                + "5.Uranus"+"\n"
                + "6.Neptune");

        System.out.print("Which planet are you visiting? ");
        choice = kb.nextInt();

        if (choice == 1) {
            System.out.println("Your weight would be " + weight*.78 + " pounds on that planet.");
        } else if (choice == 2) {
            System.out.println("Your weight would be " + weight*.39 + " pounds on that planet.");
        } else if (choice == 3) {
            System.out.println("Your weight would be " + weight*2.65 + " pounds on that planet.");

        } else if (choice == 4) {
            System.out.println("Your weight would be " + weight*1.17 + " pounds on that planet.");

        } else if (choice == 5) {
            System.out.println("Your weight would be " + weight*1.05 + " pounds on that planet.");
        } else if (choice == 6) {
            System.out.println("Your weight would be " + weight*1.23 + " pounds on that planet.");
        } else {
            System.out.println("You entered an invalid choice");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd34;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD34 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String name;
        int age;

        System.out.print("Hey what's your name? ");
        name = kb.nextLine();

        System.out.print("How old are you? ");
        age = kb.nextInt();

        if (age < 16) {
            System.out.println("You can't drive");
        } else if (age >= 16 && age <= 17) {
            System.out.println("You can drive but not vote");
        } else if (age >= 18 && age <= 24) {
            System.out.println("You can vote but you can't rent a car");
        } else {
            System.out.println("You can pretty much do anything");

        }
    }

}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd36;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD36 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        double weight, height, bmi;

        System.out.print("What is your weight? ");
        weight = kb.nextDouble();

        System.out.print("What is your height? ");
        height = kb.nextDouble();

        bmi = (weight / (height * height));

        if (bmi < 18.5) {
            System.out.println("Your BMI is " + bmi);
            System.out.println("BMI category: under weight");
        } else if (bmi >= 18.5 && bmi <= 24.9) {
            System.out.println("Your BMI is " + bmi);
            System.out.println("BMI category: normal weight");

        }
        else if (bmi >=25 && bmi <=29.9 ){
            System.out.println("Your BMI is " + bmi);
            System.out.println("BMI category: over weight");
        }
        else{
            System.out.println("Your BMI is " + bmi);
            System.out.println("BMI category: obese");
        }

    }

}

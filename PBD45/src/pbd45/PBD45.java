/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd45;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD45 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Random r = new Random();
       int die1,die2;
       
       die1= 1 + r.nextInt(6);
       die2=1+r.nextInt(6);
       
       int total=die1 + die2;
       
       System.out.println("Dice 1 is: " + die1);
       System.out.println("Dice 2 is: "+die2);
       System.out.println("Your total is: "+total);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd59;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD59 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      Scanner kb=new Scanner (System.in);
        
        int entry,number=2,count=1;
        
        
        System.out.println("I'm thinking of a number 1-10 try and guess it? ");
        
        System.out.print("Your guess: ");
        entry=kb.nextInt();
        
        do{
            count++;
            System.out.println("Incorrect, keep guessing");
            System.out.print("Your guess: ");
            entry=kb.nextInt();
            
        }while (entry!=number);
        System.out.println("You are correct!!");
        System.out.println("It only took you " + count + " times");
        
    }
    
}
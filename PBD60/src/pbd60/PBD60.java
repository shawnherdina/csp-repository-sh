/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd60;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD60 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     Scanner kb=new Scanner(System.in);
     double num=1,root=0;
     
     do{
     System.out.print("Enter a number: ");
     num=kb.nextDouble();
     
     root=Math.sqrt(num);
     
     System.out.println("The square root of " + num + " is " + root);
     
     }
     while(num>0);
    }
    
}

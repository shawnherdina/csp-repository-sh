/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbd67;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PBD67 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb=new Scanner(System.in);
        int from,to,by;
        
        System.out.print("Count from: ");
        from=kb.nextInt();
        System.out.print("Count to: ");
        to=kb.nextInt();
        System.out.print("Count by: ");
        by=kb.nextInt();
        
        for (int n=from;n>=to;n=n+(by)){
            System.out.println(n);
        }
    }
    
}

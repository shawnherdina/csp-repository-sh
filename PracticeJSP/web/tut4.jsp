<%-- 
    Document   : tut4
    Created on : Oct 13, 2015, 3:20:09 PM
    Author     : apprentice
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>
            <%!
            Date theDate = new Date();

            Date getDate() {
                System.out.println("In getDate() method");
                return theDate;
            }

            void computeDate() {
                theDate = new Date();
            }
            %>
            <%computeDate();%>
            <%%>
            Hello!  The time is now <%= getDate()%>
        </h3>
    </body>
</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Recursion {
   public static long fibonacci (long num){
       if (num <2) return num;
       return fibonacci(num-1) + fibonacci (num-2);
   }
        
    
    public static long factorial (long n){
        if (n < 0) return -1;
        if (n < 2) return 1;
        return n * factorial(n-1);
    }
    public static long factorial1(long n){
        if (n<0) return -1;
        return factorialHelper(n,1);
    }
    public static long factorialHelper(long n,long v){
        if (n < 2) return v;
        return factorialHelper(n-1, v*n);
    }
    public static void main(String[] args) {
        System.out.println(factorial(4));
        System.out.println(fibonacci(10));
        System.out.println(factorial1(4));
    }
}

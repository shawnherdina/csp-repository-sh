

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class ArrayStack<T> implements OurStack<T> {

    int next;
    T[] data;

    public ArrayStack() {
        data = (T[]) new Object[20];
        next = 0;
    }

    public void push(T t) {
        if (next == data.length) {
            growArray();
        }
        data[next] = t;
        next++;

    }

    public T peek() {
        if (isEmpty()) {
            return null;
        }
        return data[next - 1];

    }

    public T pop() {
        if (isEmpty()) {
            return null;
        }
        T temp = data[next - 1];
        data[next - 1] = null;
        next--;
        return temp;

    }

    public boolean isEmpty() {

        return next == 0;
        //return data[0] == null;
    }

    private void growArray() {
        T[] temp = (T[]) new Object[data.length * 2];
        for (int i = 0; i < data.length; i++) { //copies the array list and doubles
            temp[i] = data[i];
        }
        data = temp;
    }

    public String toString() {
        String s = "";
        for (int i = next - 1 ; i  >= 0;i--) {
            s += data [i].toString() + '\n';
        }
        return s;
    }
}

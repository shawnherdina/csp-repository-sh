/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class ListQueue<T> implements OurQueque<T> {

    private Node<T> head;
    private Node<T> tail;
    
    public void enqueue(T data) {
        if (head == null) {
            head = tail = new Node(data, null);
        } else {
            tail.setNext(new Node(data, null));
        }
        tail = tail.getNext();
    }

    public T dequeue() {
        if (head == null) return null;
        T temp = head.getData();
        if (head == tail){
            tail = head = null;
        }else{
            head = head.getNext();
        }
            
        
        return temp;
        
    }

    public T firstEl() {
        
        if (head == null) return null;
        return head.getData();
    }

    

    public boolean isEmpty() {
        return head == null;
    }

    /* public void enqueue(T data) {
     head = new Node(data, head);
     if (tail == null) {
     tail = head;
     }
     }

     public T dequeue() {
     if (head == null){
     return null;
     }
     T temp = tail.getData();
        
     if (head == tail){
     head = tail = null;
     return temp;
     }
     Node <T> curr = head;
     while (curr.getNext() != tail) {
     curr = curr.getNext();
     }
     curr.setNext(null);
     tail = curr;
     return temp;

     }

     public T firstEl() {
     return tail.getData();
     }
     */
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class ListStack<T> implements OurStack<T> {

    private Node<T> head;
    private Node<T> tail;

    public void push(T data) {
        head = new Node(data, head);
    }

    public T peek() {
        if (head == null) {
            return null;
        }
        T temp = head.getData();

        return temp;

    }

    public T pop() {

        if (head == null) {
            return null;
        }
        T temp = head.getData();

        head = head.getNext();

        return temp;
    }

   

public boolean isEmpty(){
       
        return head == null;
        
    }

    public String toString() {
        String s = "";
        for (Node curr = head;curr !=null;curr =curr.getNext()) {
            s += curr.getData().toString() + '\n';
        }
        return s;
    }
}

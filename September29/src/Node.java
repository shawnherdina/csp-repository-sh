/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Node<T> {
    private Node<T> next;
    private T data;
    
    public Node(T el, Node<T> next){
        this.next = next;
        data = el;
    }
    public T getData(){
        return data;
    }
    public void setNext(Node<T> next){
        this.next = next;
    }
    public Node<T>getNext(){
        return next;
    }
}

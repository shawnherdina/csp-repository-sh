/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public interface OurQueque<T> {
   public void enqueue(T t);
   public T dequeue();
   public T firstEl();
   public boolean isEmpty();
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public interface OurStack<T> {
    public void push (T t);
    public T peek();
    public T pop();
    public boolean isEmpty();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class September29 {

    public static void main(String[] args) {

        OurStack<String> stack = new ListStack<String>();

        stack.push("a");
        stack.push("b");
        stack.push("c");
        stack.push("d");
        stack.push("e");

        System.out.println(stack.toString());

        reverse(stack);
        System.out.println(stack.toString());

        reverse2(stack);
        System.out.println(stack.toString());

        sort(stack);
        System.out.println(stack.toString());

    }

    public static <T> void reverse(OurStack<T> o) {
        OurStack<T> s1 = new ListStack<>();
        OurStack<T> s2 = new ListStack<>();

        while (!o.isEmpty()) {
            s1.push(o.pop());
        }
        while (!s1.isEmpty()) {
            s2.push(s1.pop());
        }
        while (!s2.isEmpty()) {
            o.push(s2.pop());
        }

    }

    public static <T> void reverse2(OurStack<T> o) {
        OurStack<T> s1 = new ListStack<>();
        int size = 0;
        T temp;
        while (!o.isEmpty()) {
            s1.push(o.pop());
        }
        while (!s1.isEmpty()) {
            o.push(s1.pop());
        }
        for (int i = 0; i < size - 1; i++) {
            temp = o.pop();
            for (int j = 0; j < size - 1 - 1; j++) {
                s1.push(o.pop());
            }
            o.push(temp);
        }

        while (!s1.isEmpty()) {
            o.push(s1.pop());
        }

    }

    public static <T extends Comparable<T>> void sort(OurStack<T> o) {
        OurStack<T> s1 = new ListStack<>();
        int size = 0;
        T min;
        while (!o.isEmpty()) {
            s1.push(o.pop());
            size++;
        }
        while (!s1.isEmpty()) {
            o.push(s1.pop());
        }
        for (int i = 0; i < size - 1; i++) {
            min = o.pop();
            for (int j = 0; j < size - i - 1; j++) {
                if (o.peek().compareTo(min) < 0) {
                    s1.push(min);
                    min = o.pop();
                } else {
                    s1.push(o.pop());
                }
            }

            o.push(min);
            while (!s1.isEmpty()) {
                o.push(s1.pop());

            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab03;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SimpleCalculatorTest {
    
    public SimpleCalculatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addition method, of class SimpleCalculator.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        double num1 = 0.0;
        double num2 = 0.0;
        SimpleCalculator instance = new SimpleCalculator();
        double expResult = 0.0;
        double result = instance.addition(3, 4);
        assertEquals(7, result, 0.0);
        result = instance.addition(273, 7);
        assertEquals(280, result, 0.0);
        result = instance.addition(305, 25);
        assertEquals(330, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of subtraction method, of class SimpleCalculator.
     */
    @Test
    public void testSubtraction() {
        System.out.println("subtraction");
        double num1 = 0.0;
        double num2 = 0.0;
        SimpleCalculator instance = new SimpleCalculator();
        double expResult = 0.0;
        double result = instance.subtraction(20, 10);
        assertEquals(10, result, 0.0);
        result = instance.subtraction(35, 27);
        assertEquals (8,result,0.0);
        result = instance.subtraction (54,28);
        assertEquals (26,result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of multiplication method, of class SimpleCalculator.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        double num1 = 0.0;
        double num2 = 0.0;
        SimpleCalculator instance = new SimpleCalculator();
        double expResult = 0.0;
        double result = instance.multiplication(7, 4);
        assertEquals(28, result, 0.0);
        result = instance.multiplication(9, 4);
        assertEquals(36, result, 0.0);
        result = instance.multiplication(8, 4);
        assertEquals(32, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of division method, of class SimpleCalculator.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double num1 = 0.0;
        double num2 = 0.0;
        SimpleCalculator instance = new SimpleCalculator();
        double expResult = 0.0;
        double result = instance.division(40, 4);
        assertEquals(10, result, 0.0);
        result = instance.division(32, 8);
        assertEquals(4, result, 0.0);
        result = instance.division(100, 5);
        assertEquals(20, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}



import java.util.Scanner;

public class Cap_IO {

    public Scanner kb = new Scanner(System.in);

    public int promptInteger(String prompt) {

        int num;
        System.out.print(prompt);
        num = kb.nextInt();
        return num;

    }

    public int promptMinMaxInt(String prompt, int min, int max) {
        int num;

        do {

            System.out.print(prompt);
            System.out.print("Value must be between " + min + " and " + max + ". >>>  ");
            num = kb.nextInt();
        } while (num < min || num > max);
        return num;
    }

    public String promptString(String prompt) {

        String string;
        System.out.print(prompt);
        string = kb.nextLine();
        return string;

    }

    public float promptFloat(String prompt) {

        float num;
        System.out.print(prompt);
        num = kb.nextFloat();
        return num;

    }

    public float promptMinMaxFloat(String prompt, int min, int max) {
        float num;

        do {

            System.out.print(prompt);
            System.out.print("Value must be between " + min + " and " + max + ". >>>  ");
            num = kb.nextInt();
        } while (num < min || num > max);
        return num;
    }

    public double promptDouble(String prompt) {

        double num;
        System.out.print(prompt);
        num = kb.nextFloat();
        return num;

    }

    public double promptMinMaxDouble(String prompt, int min, int max) {
        double num;

        do {

            System.out.print(prompt);
            System.out.print("Value must be between " + min + " and " + max + ". >>>  ");
            num = kb.nextInt();
        } while (num < min || num > max);
        return num;
    }

    public void printString(String string) {
        System.out.println(string);

    }
}

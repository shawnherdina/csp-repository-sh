
import java.util.HashMap;
import java.util.Set;

class Capital {

    String name;
    int pop;
    double sqMiles;

    public Capital(String name, int pop, double sqMiles) {
        this.name = name;
        this.pop = pop;
        this.sqMiles = sqMiles;
    }
}

public class StateCapitals2 {

    public static void main(String[] args) {
        int pop = 0;

        Cap_IO io = new Cap_IO();

        HashMap<String, Capital> capitals = new HashMap<>();

        capitals.put("Alabama", new Capital("Montgomery", 205764, 155.4));
        capitals.put("Alaska", new Capital("Juneau", 31275, 2716.7));
        capitals.put("Arizona", new Capital("Phoenix", 1445632, 474.9));
        capitals.put("Arkansas", new Capital("Little Rock", 193524, 116.2));
        capitals.put("California", new Capital("Sacramento", 466488, 97.2));
        capitals.put("Colorado", new Capital("Denver", 600158, 153.4));
        capitals.put("Connecticut", new Capital("Hartford", 124512, 17.3));
        capitals.put("Deleware", new Capital("Dover", 36047, 22.4));
        capitals.put("Florida", new Capital("Tallahassee", 181412, 95.7));
        capitals.put("Georgia", new Capital("Atlanta", 420003, 131.7));
        capitals.put("Hawaii", new Capital("Honolulu", 337256, 85.7));
        capitals.put("Idaho", new Capital("Boise", 205671, 63.8));
        capitals.put("Illinois", new Capital("Springfield", 116250, 54.0));
        capitals.put("Indiana", new Capital("Indianapolis", 829718, 361.5));
        capitals.put("Iowa", new Capital("Des Moines", 203433, 75.8));
        capitals.put("Kansas", new Capital("Topeka", 127473, 56.0));
        capitals.put("Kentucky", new Capital("Frankfort", 25527, 14.7));
        capitals.put("Louisiana", new Capital("Baton Rouge", 229553, 76.8));
        capitals.put("Maine", new Capital("Augusta", 19136, 55.4));
        capitals.put("Maryland", new Capital("Annapolis", 38394, 6.73));
        capitals.put("Massachusetts", new Capital("Boston", 617594, 48.4));
        capitals.put("Michigan", new Capital("Lansing", 114297, 35.0));
        capitals.put("Minnesota", new Capital("St Paul", 285068, 52.8));
        capitals.put("Mississippi", new Capital("Jackson", 173514, 104.9));
        capitals.put("Missouri", new Capital("Jefferson City", 43079, 27.3));
        capitals.put("Montana", new Capital("Helena", 28190, 14.0));
        capitals.put("Nebraska", new Capital("Lincoln", 258379, 74.6));
        capitals.put("Nevada", new Capital("Carson City", 55274, 143.4));
        capitals.put("New Hampshire", new Capital("Concord", 42695, 64.3));
        capitals.put("New Jersey", new Capital("Trenton", 84913, 7.66));
        capitals.put("New Mexico", new Capital("Santa Fe", 75764, 37.3));
        capitals.put("New York", new Capital("Albany", 97856, 21.4));
        capitals.put("North Carolina", new Capital("Raleigh", 403892, 114.6));
        capitals.put("North Dakota", new Capital("Bismark", 61272, 26.9));
        capitals.put("Ohio", new Capital("Columbus", 822553, 210.3));
        capitals.put("Oklahoma", new Capital("Oklahoma City", 580000, 607));
        capitals.put("Oregon", new Capital("Salem", 154637, 45.7));
        capitals.put("Pennsylvania", new Capital("Harrisburg", 49528, 8.11));
        capitals.put("Rhode Island", new Capital("Providence", 178042, 18.5));
        capitals.put("South Carolina", new Capital("Columbia", 131686, 125.2));
        capitals.put("South Dakota", new Capital("Pierre", 13646, 13));
        capitals.put("Tennessee", new Capital("Nashville", 635710, 473.3));
        capitals.put("Texas", new Capital("Austin", 790390, 251.5));
        capitals.put("Utah", new Capital("Salt Lake City", 186440, 109.1));
        capitals.put("Vermont", new Capital("Montpelier", 7855, 10.2));
        capitals.put("Virginia", new Capital("Richmond", 204214, 60.1));
        capitals.put("Washington", new Capital("Olympia", 46478, 16.7));
        capitals.put("West Virginia", new Capital("Charleston", 51400, 31.6));
        capitals.put("Wisconsin", new Capital("Madison", 233209, 68.7));
        capitals.put("Wyoming", new Capital("Cheyenne", 59466, 21.1));

        pop = io.promptInteger("Enter a minimum State Capital population: ");

        Set<String> keys = capitals.keySet();

        for (String k : keys) {
            if (capitals.get(k).pop > pop) {
                System.out.println(k + " - " + capitals.get(k).name + " | Pop: " + capitals.get(k).pop + " | Area: " + capitals.get(k).sqMiles + " sq mi");
            }

        }
    }
}

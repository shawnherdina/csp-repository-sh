import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class StudentQuizGrades {


    int value;

    public StudentQuizGrades() {

    }

    public int mainMenuMeth() {
        Quiz_IO io = new Quiz_IO();
        int answer = 0;
        value = io.promptMinMaxInt("Please select the corresponding number of your choice: \n"
                + "1. View List of Students\n"
                + "2. Add a Student\n"
                + "3. Remove a Student\n"
                + "4. Student Quiz Scores\n"
                + "5. Average Student Quiz Score\n"
                + "6. Average Quiz Entire Class \n"
                + "7. Student With Highest Quiz Score\n"
                + "8. Student With Lowest Quiz Score\n"
                + "9. Exit\n",
                1, 9
        );
        answer = value;
        return answer;
    }

    public static void main(String[] args) {
        Quiz_IO io = new Quiz_IO();
        HashMap<String, ArrayList<Double>> quiz = new HashMap<>();
        int mainMenu = 0;
        double avg = 0, scores = 0;
        Set<String> keys = quiz.keySet();
        String student = "";
        StudentQuizGrades sQg = new StudentQuizGrades();

        ArrayList<Double> totScores = new ArrayList();

        do {

            mainMenu = sQg.mainMenuMeth();

            switch (mainMenu) {
                case 1:
                    keys = quiz.keySet();
                    System.out.println("Students: ");
                    for (String k : keys) {
                        System.out.println(k);
                    }

                    break;
                case 2:
                    student = io.promptString("Student Name: ");
                    ArrayList<Double> qScores = new ArrayList();
                    while (scores >= 0) {
                        scores = io.promptInteger("Enter quiz score (NEGATIVE SCORE EXITS) : ");
                        if (scores >= 0) {
                            qScores.add(scores);
                        }

                    }
                    scores = 0;
                    quiz.put(student, qScores);

                    break;
                case 3:
                    student = io.promptString("Student Name: ");
                    quiz.remove(student);

                    break;
                case 4:
                    student = io.promptString("Student Name: ");

                    System.out.println("The quiz scores are: " + quiz.get(student) + "\n");

                    break;
                case 5:
                    student = io.promptString("Student Name: ");

                    ArrayList<Double> qScoresAvg = new ArrayList();
                    qScoresAvg = quiz.get(student);
                    Iterator<Double> iter = qScoresAvg.iterator();
                    double total = 0;
                    while (iter.hasNext()) {
                        total += iter.next();
                    }
                    avg = total / qScoresAvg.size();
                    System.out.println("Student " + student + " quiz score average: " + avg + "\n");

                    break;
                case 6:
                    double sum = 0;
                    double totSc = 0;
                    for (String k : keys) {
                        totScores = quiz.get(k);
                        for (Double j : totScores) {
                            sum += j;

                        }
                        totSc += totScores.size();

                        avg = sum / totSc;
                    }
                    System.out.println("Average quiz scores: " + avg + "\n");
                    break;
                case 7:
                    Double highScore = 0.;
                    String highStudent = "";
                    for (String k : keys) {
                        totScores = quiz.get(k);
                        for (Double j : totScores) {
                            j = Collections.max(totScores);
                            //highScore = totScores.get(j);
                            if (j > highScore) {
                                highScore = j;
                                highStudent = k;
                               
                            }

                        }

                    }
                    System.out.println(highStudent + " has the highest quiz score with a: " + highScore + ".");
                    break;
                case 8:
                    Double lowScore = 100.;
                    String lowStudent = "";
                    for (String k : keys) {
                        totScores = quiz.get(k);
                        for (Double j : totScores) {
                            j = Collections.min(totScores);
                            //highScore = totScores.get(j);
                            if (j < lowScore) {
                                lowScore = j;
                                lowStudent = k;
                            }

                        }

                    }
                    System.out.println(lowStudent + " has the lowest quiz score with a: " + lowScore + ".");
                    break;
                default:

            }
        } while (mainMenu != 9);

    }
}

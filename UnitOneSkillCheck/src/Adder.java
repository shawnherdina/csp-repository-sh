
public class Adder {

    public static int integerSum(int num1, int num2) {
        int sum;

        sum = num1 + num2;

        return sum;
    }

    public static void main(String[] args) {

        System.out.println(integerSum(1, 1));
        System.out.println(integerSum(2, 3));
        System.out.println(integerSum(5, 8));
        System.out.println(integerSum(95, 42));

    }

}

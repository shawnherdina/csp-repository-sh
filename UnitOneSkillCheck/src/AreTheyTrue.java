
public class AreTheyTrue {

    public static String howTrue(boolean firstTest, boolean secondTest) {
        if (firstTest != true && secondTest != true) {

            return "Neither";
        } else if (firstTest == true && secondTest == true) {

            return "Both";
        } else if (firstTest == true || secondTest == true) {

            return "Only one";

        } else {

            return "Error";
        }
    }

    public static void main(String[] args) {
        System.out.println(howTrue(false, false));
        System.out.println(howTrue(true, true));
        System.out.println(howTrue(true, false));
        System.out.println(howTrue(false, true));

    }

}


import java.util.Scanner;

public class WhatIsYourNumber {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int number;

        System.out.print("Enter an integer: ");
        number = kb.nextInt();

        System.out.println("Your integer is: " + number);

        for (int n = 0; n <= number; n++) {
            
            System.out.println(n);

        }

        System.out.println("Thank You For Playing!!");
    }

}

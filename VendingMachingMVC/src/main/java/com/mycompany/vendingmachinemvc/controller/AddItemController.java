/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinemvc.controller;

import com.mycompany.vendingmachinemvc.model.Vending;
import com.mycompany.vendingmachinemvc.dao.VendingListDao;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller

/**
 *
 * @author apprentice
 */
public class AddItemController {
    
     private VendingListDao dao;
     
    @Inject
    public AddItemController(VendingListDao dao) {
        this.dao = dao;
}
     
     @RequestMapping(value = "/additem", method = RequestMethod.GET)
    public String displayHomePage() {
        return ("addItem");
    }
    
    @RequestMapping(value = "/additem", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Vending createItem(@Valid @RequestBody Vending vending) {
        dao.addItem(vending);
        return vending;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinemvc.controller;

import com.mycompany.vendingmachinemvc.dao.VendingListDao;
import com.mycompany.vendingmachinemvc.model.Vending;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;



/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {
     private VendingListDao dao;

    @Inject
    public HomeController(VendingListDao dao) {
        this.dao = dao;
    }
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return ("home");
    }
    @RequestMapping(value = "/vendings", method = RequestMethod.GET)
    @ResponseBody
    public List<Vending> getAllItems() {
        return dao.getAllItems();
    }
    @RequestMapping(value="/vending/{id}",method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putVending(@PathVariable("id")int id,@RequestBody Vending vending) {
        vending.setVendingId(id);
        dao.updateItem(vending);
    }
    @RequestMapping(value = "/vending/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Vending getVending(@PathVariable("id") int id) {
        return dao.getVendingsById(id);
    }
    

}

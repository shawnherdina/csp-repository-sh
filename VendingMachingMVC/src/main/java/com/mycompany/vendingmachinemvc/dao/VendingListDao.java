/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinemvc.dao;

import com.mycompany.vendingmachinemvc.dao.SearchTerm;
import com.mycompany.vendingmachinemvc.model.Vending;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface VendingListDao {

    public Vending addItem(Vending vending);
    
    public void removeItem(int vendingId);

    public List<Vending> getAllItems();
    
    public void updateItem(Vending vending);
    
    public Vending getVendingsById(int vendingId);
    
    public List<Vending> searchItems(Map<SearchTerm, String> criteria);
    
    

}

    
  
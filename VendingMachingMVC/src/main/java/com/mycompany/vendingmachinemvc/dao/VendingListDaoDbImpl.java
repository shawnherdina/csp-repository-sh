/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinemvc.dao;
import com.mycompany.vendingmachinemvc.model.Vending;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class VendingListDaoDbImpl implements VendingListDao {
    private static final String SQL_INSERT_VENDING
            = "insert into vending (item, cost, quantity) values (?, ?, ?)";

    private static final String SQL_DELETE_VENDING
            = "delete from vending where vending_id = ?";

    private static final String SQL_SELECT_VENDING
            = "select * from vending where vending_id = ?";

    private static final String SQL_SELECT_ALL_VENDINGS
            = "select * from vending";

    private static final String SQL_UPDATE_VENDING
            = "update vending set item = ?, cost = ?, quantity = ?-1";

    private JdbcTemplate jdbcTemplate;

    public void setjdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Vending addItem(Vending vending) {
        jdbcTemplate.update(SQL_INSERT_VENDING,
                vending.getItem(),
                vending.getCost(),
                vending.getQuantity());
                
        vending.setVendingId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return vending;
    }

    @Override
    public void removeItem(int vendingId) {
        jdbcTemplate.update(SQL_DELETE_VENDING, vendingId);
    }


    @Override
    public List<Vending> getAllItems() {
        return jdbcTemplate.query(SQL_SELECT_ALL_VENDINGS, new VendingMapper());
    }
    
     @Override
    public void updateItem(Vending vending) {
        jdbcTemplate.update(SQL_UPDATE_VENDING,
                vending.getItem(),
                vending.getCost(),
                vending.getQuantity());
    }

    @Override
    public Vending getVendingsById(int vendingId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_VENDING, new VendingMapper(), vendingId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Vending> searchItems(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllItems();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from vending where ");

            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];

            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();
            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }

                sQuery.append(currentKey);
                sQuery.append(" = ? ");

                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }

            return jdbcTemplate.query(sQuery.toString(), new VendingMapper(), paramVals);
        }
    }

    private static final class VendingMapper implements ParameterizedRowMapper<Vending> {

        @Override
        public Vending mapRow(ResultSet rs, int i) throws SQLException {
            Vending vend = new Vending();
            vend.setVendingId(rs.getInt("vending_id"));
            vend.setItem(rs.getString("item"));
            vend.setCost(rs.getInt("cost"));
            vend.setQuantity(rs.getInt("quantity"));
            return vend;
        }

    }

}

    



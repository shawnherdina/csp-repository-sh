/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinemvc.dao;

import com.mycompany.vendingmachinemvc.model.Vending;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class VendingListDaoInMemImpl implements VendingListDao {
  private Map<Integer, Vending> vendingMap = new HashMap<>();

    private static int vendingIdCounter = 0;

    @Override
    public Vending addItem(Vending vending) {
        vending.setVendingId(vendingIdCounter);
        vendingIdCounter++;
        vendingMap.put(vending.getVendingId(), vending);
        return vending;
    }
    @Override
    public void removeItem(int vendingId) {
        vendingMap.remove(vendingId);
    }

    @Override
    public List<Vending> getAllItems() {
        Collection<Vending> c = vendingMap.values();
        return new ArrayList(c);
    }

    @Override
    public Vending getVendingsById(int vendingId) {
        return vendingMap.get(vendingId);
    }
    
    public void resetQuantity(Vending quantity) {
        int num = quantity.getQuantity();
        num = num - 1;
        quantity.setQuantity(num);
        vendingMap.put(quantity.getVendingId(), quantity);
    }
    
    @Override
    public void updateItem(Vending vending) {
        vendingMap.put(vending.getVendingId(), vending);
    }
    
    @Override
    public List<Vending> searchItems(Map<SearchTerm, String> criteria) {
        String itemCriteria = criteria.get(SearchTerm.ITEM);
        String costCriteria = criteria.get(SearchTerm.COST);
        String quantityCriteria = criteria.get(SearchTerm.QUANTITY);

        Predicate<Vending> itemMatches;
        Predicate<Vending> costMatches;
        Predicate<Vending> quantityMatches;

        Predicate<Vending> truePredicate = (c) -> {
            return true;
        };
        itemMatches = (itemCriteria == null || itemCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getItem().equals(itemCriteria);
        costMatches = (costCriteria == null || costCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getCost()==Integer.parseInt(costCriteria);
        quantityMatches = (quantityCriteria == null || quantityCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getQuantity()==Integer.parseInt(quantityCriteria);
        return vendingMap.values().stream()
                .filter(itemMatches
                        .and(costMatches)
                        .and(quantityMatches))
                .collect(Collectors.toList());
    }

}

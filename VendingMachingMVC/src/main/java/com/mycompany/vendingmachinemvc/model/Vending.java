/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vendingmachinemvc.model;

import java.util.Objects;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Vending {

    private int vendingId;

    @NotEmpty(message = "You must supply a value for an Item.")
    @Length(max = 50, message = "Item must be no more than 50 characters in length.")
    private String item;

    @Min(value = 1,message = "You must supply a value for an Item.")
    @Max(value = 99, message = "Quantity must be no more than 2 characters in length.")
    private int quantity;

    @Min(value=1,message="You must supply a value for Cost.")
    @Max(value = 99, message = "Cost must be no more than 2 characters in length.")
    private int cost;
    
   
    public int getVendingId() {
        return vendingId;
    }

    public void setVendingId(int vendingId) {
        this.vendingId = vendingId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.vendingId;
        hash = 37 * hash + Objects.hashCode(this.item);
        hash = 37 * hash + Objects.hashCode(this.cost);
        hash = 37 * hash + Objects.hashCode(this.quantity);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vending other = (Vending) obj;
        if (this.vendingId != other.vendingId) {
            return false;
        }
        if (!Objects.equals(this.item, other.item)) {
            return false;
        }
        if (!Objects.equals(this.cost, other.cost)) {
            return false;
        }
        if (!Objects.equals(this.quantity, other.quantity)) {
            return false;

        }
        return true;
    }
}

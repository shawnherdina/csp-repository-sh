/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    loadVendingMachines();
});
$('#add-button').click(function (event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'additem',
        data: JSON.stringify({
            item: $('#add-item').val(),
            cost: $('#add-cost').val(),
            quantity: $('#add-quantity').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#add-item').val('');
        $('#add-cost').val('');
        $('#add-quantity').val('');
        $('#validationErrors').empty();
        loadVendingMachines();
    }).error(function (data, status) {
        $.each(data.responseJSON.fieldErrors, function (index, validationError) {
            var errorDiv = $('#validationErrors');
            errorDiv.append(validationError.message).append($('<br>'));
        });
    });
});


function loadVendingMachines() {
    $.ajax({
        url: "vendings"
    }).success(function (data, status) {
        fillVendingTable(data, status);
    });

}
function fillVendingTable(tableList, status) {
    clearVendingTable();
    var cTable = $('#contentRows');
    $.each(tableList, function (index, vending) {
        cTable.append($('<tr>')
                .append($('<td>').text(vending.item))
                .append($('<td>').text(vending.cost))
                .append($('<td>').text(vending.quantity))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'purchaseItem(' +
                                            vending.vendingId + ')'
                                }).text('Purchase'))
                        )
                );
    });
}


function purchaseItem(id) {
    $.ajax({
        type: 'GET',
        url: 'vending/' + id
    }).success(function (vending) {
        if (vending.quantity <= 0) {
            alert("We are currently out of that item, Please make another selection");

        } else {
            if ($('#add-money').val() < vending.cost) {
                alert("Please enter more credits");
            } else {
                var answer = confirm("Is this the item you really want to purchase?");
                if (answer === true)
                    $.ajax({
                        type: 'PUT',
                        url: 'vending/' + id,
                        data: JSON.stringify({
                            vendingId: id,
                            item: vending.item,
                            cost: vending.cost,
                            quantity: vending.quantity

                        }),
                        headers: {
                            'Accept': 'application/json', 
                            'Content-Type': 'application/json'
                        },
                        'dataType':'json'
                    }).success(function () {
                    message = alert("You have successfully purchase this item");
                    clearChangeTable();
                    var ctable=$('#changeRows');
                    ctable.append($('#add-money').val()- vending.cost);
                    $('#add-money').val('')
                        loadVendingMachines();
                    });
            }
        }
    });
}
function clearChangeTable(){
    $('#changeRows').empty();
}
function clearVendingTable() {
    $('#contentRows').empty();
}










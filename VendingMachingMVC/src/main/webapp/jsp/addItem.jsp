<%-- 
    Document   : addItem
    Created on : Oct 23, 2015, 3:17:42 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>VENDING MACHINE</h1>
            <hr/>
            <div class="navbar">
                <ul class ="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/additem">Add Items</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 ">
                <h2 class ="col-md-offset-4">Add New Item</h2><br>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="add-item" class="col-md-4 control-label">
                            Item:
                        </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-item" placeholder="Item"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-cost" class="col-md-4 control-label">
                            Credits:
                        </label>
                        <div class="col-md-8">
                            <input type="number" class="form-control" id="add-cost" placeholder="Cost"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-quantity" class="col-md-4 control-label">
                            Quantity:
                        </label>
                        <div class="col-md-8">
                            <input type="number" class="form-control" id="add-quantity" placeholder="Quantity"/>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-md-offset-4 clo-md-8">
                            <button type="submit" id="add-button" class="btn btn-default">
                                Add Item
                            </button>
                        </div>
                    </div>
                </form>
                <div id="validationErrors" style="color: red"></div>
            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vendingMachineList.js"></script>
    </body>
</html>

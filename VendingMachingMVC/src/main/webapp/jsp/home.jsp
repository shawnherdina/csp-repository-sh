<%-- 
    Document   : home
    Created on : Oct 23, 2015, 2:43:12 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>VENDING MACHINE</h1>
            <hr/>
            <div class="navbar">
                <ul class ="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/additem">Add Items</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>Items Available</h2>
                    <table id="contactTable" class="table table-hover">
                        <tr>
                            <th width="30%">ITEM</th>
                            <th width="20%">CREDITS</th>
                            <th width="20%">QUANTITY</th>
                            <th width="20%"></th>
                        </tr>
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <h2>Insert Money</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-money" class="col-md-4 control-label">
                                Insert Money:
                                <BR>
                                $1.00 = 100 Credits
                            </label>
                            <div class="col-md-8">
                                <input type="number"
                                       class="form-control"
                                       id="add-money"
                                       placeholder="Insert Money"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
<!--                                <button type="submit"
                                        id="add-button"
                                        class="btn btn-default">
                                    Insert Money
                                </button>-->
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <h3>Change</h3>
                                <table id="contactTable" class="table table-hover">
                                    <tr>
                                        <th width="100%"></th>
                                    </tr>
                                    <tbody id="changeRows"></tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                    <div id="validationErrors" style="color: red"></div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vendingMachineList.js"></script>
    </body>
</html>

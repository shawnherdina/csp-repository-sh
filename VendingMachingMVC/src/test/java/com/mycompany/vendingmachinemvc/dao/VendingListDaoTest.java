/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package com.mycompany.vendingmachinemvc.dao;
//
//import com.mycompany.vendingmachinemvc.model.Vending;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import static junit.framework.TestCase.assertEquals;
//import static junit.framework.TestCase.assertNull;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author apprentice
// */
//public class VendingListDaoTest {
//    
//    private VendingListDao dao;
//    
//    public VendingListDaoTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    @Test
//    public void addGetDeleteVending() {
//
//        Vending nc = new Vending();
//        nc.setItem("ET");
//        nc.setCost(2);
//        nc.setQuantity(2);
//        dao.addItem(nc);
//        Vending fromDb = dao.getVendingsById(nc.getVendingId());
//        assertEquals(fromDb, nc);
//        dao.removeItem(nc.getVendingId());
//        assertNull(dao.getVendingsById(nc.getVendingId()));
//    }
//
//    @Test
//    public void addUpdateVending() {
//        
//        Vending nc = new Vending();
//        nc.setItem("Jaws");
//        nc.setCost(2);
//        nc.setQuantity(2);
//        dao.addItem(nc);
//        Vending fromDb = dao.getVendingsById(nc.getVendingId());
//        assertEquals(fromDb, nc);
//    }
//
//    @Test
//    public void getAllVendingses() {
//
//        Vending nc = new Vending();
//        nc.setItem("Jaws");
//        nc.setCost(2);
//        nc.setQuantity(2);
//        dao.addItem(nc);
//
//        Vending nc2 = new Vending();
//        nc2.setItem("Dumbo");
//        nc2.setCost(5);
//        nc2.setQuantity(2);
//        dao.addItem(nc2);
//        List<Vending> cList = dao.getAllItems();
//        assertEquals(cList.size(), 2);
//    }
//
//    @Test
//    public void searchVending() {
//
//        Vending nc = new Vending();
//        nc.setItem("Jaws");
//        nc.setCost(2);
//        nc.setQuantity(2);
//        dao.addItem(nc);
//
//        Vending nc2 = new Vending();
//        nc2.setItem("Dumbo");
//        nc2.setCost(5);
//        nc2.setQuantity(2);
//        dao.addItem(nc2);
//
//        Vending nc3 = new Vending();
//        nc3.setItem("Avatar");
//        nc3.setCost(4);
//        nc3.setQuantity(2);
//        dao.addItem(nc3);

//        Map<SearchTerm, String> criteria = new HashMap<>();
//        criteria.put(SearchTerm.QUANTITY, 2);
//        List<Vending> cList = dao.searchItems(criteria);
//        assertEquals(1, cList.size());
//        assertEquals(nc2, cList.get(0));
//
//        criteria.put(SearchTerm.QUANTITY, 2);
//        cList = dao.searchItems(criteria);
//        assertEquals(2, cList.size());
//
//        criteria.put(SearchTerm.COST, 2);
//        cList = dao.searchItems(criteria);
//        assertEquals(1, cList.size());
//        assertEquals(nc, cList.get(0));
//
//        criteria.put(SearchTerm.COST, 4);
//        cList = dao.searchItems(criteria);
//        assertEquals(1, cList.size());
//        assertEquals(nc3, cList.get(0));
//
//        criteria.put(SearchTerm.COST, 5);
//        cList = dao.searchItems(criteria);
//        assertEquals(0, cList.size());
//        criteria.put(SearchTerm.COST, 5);
//        cList = dao.searchItems(criteria);
//        assertEquals(0, cList.size());
//    }
//}

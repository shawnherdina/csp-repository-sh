/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windowmasterv2;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowmasterV2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // TODO code application logic here
        float height, width;
        float area, perimeter;
        float GlassCost, TrimCost, TotalCost;

        Scanner kb = new Scanner(System.in);

        System.out.print("Enter the height: ");
        height = kb.nextFloat();

        System.out.print("Enter the width: ");
        width = kb.nextFloat();

        area = height * width;
        perimeter = (height + width) * 2;
        TrimCost = perimeter * 2.25f;
        GlassCost = area * 3.5f;
        TotalCost = GlassCost + TrimCost;

        System.out.println("Area is " + area);
        System.out.println("Perimeter is " + perimeter);
        System.out.println("Glass cost is " + GlassCost);
        System.out.println("Trim cost is " + TrimCost);
        System.out.println("Windows total cost is " + TotalCost);
    }

}

        // TODO code application logic here

